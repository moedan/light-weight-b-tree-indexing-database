Manual:
	1. open the terminal
	2. use command 'cd' to get into the program folder 
	3. type 'make clean' to erase potentially existing redundant files
	4. type 'make' to compile
	5. type './test_assign2_1' to see the results

Notes:
	1.for each file we have one and only one buffer pool
	2.during the pinPage function, three cases will be encountered
		 ~ the page is in the frames, then return the load that frame to the page handle then update LRU list
		 ~ the frames are still not full. Load the page from file into the frame and the page handle then update FIFO list and LRU list
		 ~ the frames are full. This is where strategy replacement happens. Based on the strategy buffer uses, find the first frame with fixCounts==0 in the list, the replace it with the desired page from file, and load the frame to page handle
	
Main algorithm for FIFO and LRU:
     FIFO: we maintain a single linked list to decide which frame to be replaced.
     LRU: we also maintain a single linked list to decide the frame to be repalced.

Main structure:
     BM_BufferPool: Representing a buffer pool, containing information about the file name,total page numbers, replacement strategy, 
     		    management info( used to keep track of frame list), total reading numbers, total writing numbers, FIFO list, LRU list.
     BM_Frame: representing a frame
     	       isDirty: indicating whether the frame is not updated in page file
	       fixCounts: indicating how many user are pinning the page
	       pageNum: indicating which page of the file is loaded into the frame
	       data: contain the data of the page
     BM_PageHandle: representing a page
     		    pageNumber: the page's number in the file
		    data: the pointer to the data field
     list: linked list to fulfill the manipulations of FIFO and LRU strategy
     	   


Our module main consists of three parts:
     pool_controller.c : Buffer pool interface manager including initiate the buffer pool, destroy the buffer pool, write the contents in pool back into the file 
			 initBufferPool: initiate the buffer pool, allocate memory for the frames
		       	 shutdownBufferPool: free the memories of the buffer pool
		       forceFlushPool: wirte the data in frame back into the file
     page_frame_controller.c: buffer manager interface to pages
     			      findPage: using the page number to find the index of frame which contains the specific page
			      load_frame_to_page:load a frame to a page handle
			      find_empty_cache:find the cache with page number -1
			      markDirty: mark the frame to be dirty
			      unpinPage: client quit using a page
			      forcePage: write the frame's content back to the page file
			      pinPage: client start using a page
     frame_info_controller.c: statistics about frames
     			      getFrameContents: function returns an array of PageNumbers (of size numPages) where the ith element is the number of the page stored in the ith page frame. An empty page frame is represented using the constant NO_PAGE.
			      getDirtyFlags:function returns an array of bools (of size numPages) where the ith element is TRUE if the page stored in the ith page frame is dirty. Empty page frames are considered as clean.
			      getDirtyFlags:function returns an array of ints (of size numPages) where the ith element is the fix count of the page stored in the ith page frame. Return 0 for empty page frames.
			      getNumReadIO: function returns the number of pages that have been read from disk since a buffer pool has been initialized.
		              getNumWriteIO: returns the number of pages written to the page file since the buffer pool has been initialized.
     