#include "buffer_mgr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>

int findPage(BM_BufferPool *const bm, PageNumber pageNum){
  
   PageNumber * pn = getFrameContents(bm); //get the frame contents of buffer pool
   int i;
  
  for(i = 0; i< bm->numPages;i++){ 
    if(pn[i] == pageNum) return i;
  } //this loop gets the frame with specified pageNum
  
  return -1;
  
}

/* load a frame in memory to a page, set up page number and data*/
void load_frame_to_page(BM_Frame * const fList, int i, BM_PageHandle *const page){
  page->pageNum = fList[i].pageNum;
  page->data = fList[i].data;
  
}


int findEmptyCache(BM_BufferPool *const bm){
	int i; //variable for loop

	BM_Frame *fList = (BM_Frame*)bm->mgmtData; //load frame
	for(i = 0; i < bm->numPages; i++){
 		if(fList[i].pageNum == -1) break;
	}
	if(i < bm->numPages){
		return i;
	}else{
		return -1;
	}
}



// Buffer Manager Interface Access Pages
RC markDirty (BM_BufferPool *const bm, BM_PageHandle *const page){
  
  BM_Frame *fList;
  int i = findPage(bm, page->pageNum);
 //the page is not in the frames
  if(i == -1)  return RC_BM_INVALID_PAGE;
  
  fList = (BM_Frame*)(bm->mgmtData);
 // Make the page dirty
  fList[i].isDirty = true;
  return RC_OK; 
}


RC unpinPage (BM_BufferPool *const bm, BM_PageHandle *const page){
    
  BM_Frame *fList;	
  int i = findPage(bm,page->pageNum);
//the page is not in the frames
  if(i == -1)  return RC_BM_INVALID_PAGE;
	
  fList = (BM_Frame*)(bm->mgmtData);
  fList[i].fixCounts--;
 //flush the page to the disk if dirty
  if( fList[i].isDirty == true){
    forcePage(bm, page);
    fList[i].isDirty = false;
  }	
  return RC_OK;
	    
}
RC forcePage (BM_BufferPool *const bm, BM_PageHandle *const page){


  SM_FileHandle fh; 
  BM_Frame *fList;

  openPageFile(bm->pageFile, &fh);

  int i = findPage(bm, page->pageNum);


  fList = (BM_Frame*)(bm->mgmtData);

  if(fList[i].fixCounts > 0) return RC_BM_PAGE_IS_PINNED;

  writeBlock(page->pageNum, &fh, page->data);


  bm->numWrite++;                  //another write operation from frame to file

  fList[i].isDirty = false;
  closePageFile(&fh);
  return RC_OK;

}

RC pinPage (BM_BufferPool *const bm, BM_PageHandle *const page, const PageNumber pageNum){
 

  /* file handler used to read frame from file */

  SM_FileHandle fh;
  BM_Frame *fList;
  fList = (BM_Frame*)(bm->mgmtData);




  int i = findPage(bm,pageNum);   // Assign pageNum to i
  int j = findEmptyCache(bm); // Assign EmptyCache num to j
  page->pageNum = pageNum;
  if(i != -1){/* First case: the requested page is already in the cache (frame)  */
    load_frame_to_page(fList, i, page);
    page->data = fList[i].data;
    page->pageNum = pageNum;
    fList[i].fixCounts++; // Simply return the page in this case

    switch(bm->strategy){
       case RS_FIFO: break;
       case RS_LRU:{
          list_remove_element_X(bm->ll,i);           /* remove the frame from the LRU list*/
          list_add_element(bm->ll,i);              /* add the frame to the tail of the LRU list*/
          break;
       }
       case RS_CLOCK:{
          bm->clock[pageNum] = true;
	  break;
       }
       case RS_LFU:{
          fList[i].frequency++; // if RS_LFU applies, frequency+1
	  break;
       }
    }

  }else if(j != -1){  /* Second case: page is not in the cache, and the frame has vacancy */

    openPageFile(bm->pageFile, &fh);

    fList[j].pageNum = pageNum;
    readBlock(pageNum, &fh, fList[j].data);
    load_frame_to_page(fList, j, page);     /* load the frame to page */
    fList[j].fixCounts++;
   
    switch(bm->strategy){
       case RS_FIFO:{
	  list_add_element(bm->fl,j);             /* add the frame to the tail of the FIFO list*/

          break;
       }
       case RS_LRU:{
          list_add_element(bm->ll,j);		    /* add the frame to the tail of the LRU list*/
          break;
       }
       case RS_CLOCK:{
          bm->clock[pageNum] = true;
	  break;
       }
       case RS_LFU:{
          fList[j].frequency = 1;	// Since this is the first time to load the page, frequency is set to 1
	  break;
       }
    }
    bm->numRead++;
    closePageFile(&fh);

  }else{  /* Third case: page is not in the cache, but the cache is full and replacement strategy is required */


	openPageFile(bm->pageFile, &fh);

	switch (bm->strategy){
    
		case  RS_FIFO:{
       			Node * node = bm->fl->head;           /* get the FIFO list head node */
       
       			while(node != NULL){
	 			int cache_index = node->cache_index;
	 			if(fList[cache_index].fixCounts ==0){           /* do the loop only if the fixCounts of the frame is zero*/
	   
	   				list_remove_element_X(bm->fl,cache_index);     /* find and remove the node from list*/
	   				list_add_element(bm->fl,cache_index);          /* add the node back to the tail of the list */

	   				/* if the frame is dirty, flush it first */
	   				if(fList[cache_index].isDirty == true){
						writeBlock(fList[cache_index].pageNum, &fh, fList[cache_index].data);

						fList[cache_index].isDirty = false;
						bm->numWrite++;	
					}
					readBlock(pageNum, &fh, fList[cache_index].data);
					/* load the new frame from the file */
				   	fList[cache_index].pageNum = pageNum;
	   				fList[cache_index].fixCounts++;
	   				/* load the frame to pageHandle */
	   				load_frame_to_page(fList,cache_index,page);
	   				bm->numRead++;
					closePageFile(&fh);
	   				return RC_OK;
	 			}
	 			/*the current node(frame) is in use, try next node*/
	 			node = node->next;	 
       			}
       
       			/* all frames are in use, buffer full */
       			return RC_BM_BUFFER_FULL;   
     		}

    		case RS_LRU:{
    
      			Node * node = bm->ll->head;           /* get the LRU list head node, which is the least recently accessed page */
       
      			while(node != NULL){
				int cache_index = node->cache_index;
				if(fList[cache_index].fixCounts ==0){           /* the fixCounts of the frame is zero*/
	   
	  				list_remove_element_X(bm->ll,cache_index);       /* find and remove the node from list*/
	  				list_add_element(bm->ll,cache_index);          /* add the node back to the tail of the list */


					/* if the frame is dirty, flush it first */
	   				if(fList[cache_index].isDirty == true){
						writeBlock(fList[cache_index].pageNum, &fh, fList[cache_index].data);

						fList[cache_index].isDirty = false;
						bm->numWrite++;	
					}
					readBlock(pageNum, &fh, fList[cache_index].data);
					/* load the new frame from the file */
				   	fList[cache_index].pageNum = pageNum;
	   				fList[cache_index].fixCounts++;
	   				/* load the frame to pageHandle */
	   				load_frame_to_page(fList,cache_index,page);
	   				bm->numRead++;
					closePageFile(&fh);
	   				return RC_OK;

				}
				/*the current node(frame) is in use, try next node*/
				node = node->next;	 
      			}
       
      			/* all frames are in use, buffer full */
      			return RC_BM_BUFFER_FULL;   
    		}
	        case RS_CLOCK:{
			for(;; bm->pointer = (bm->pointer + 1 ) % bm->numPages){
				if(fList[bm->pointer].fixCounts > 0) continue;
				if(bm->clock[bm->pointer] == false) break;
				bm->clock[bm->pointer] = false;
			}//This loop moves the clock pointer to the first available frame in the cache whose fixCounts is non-zero, and sets all during the scan as available if its fixCounts is 0
			/* if the frame is dirty, flush it first */
			if(fList[bm->pointer].isDirty == true){
				writeBlock(fList[bm->pointer].pageNum, &fh, fList[bm->pointer].data);
				fList[bm->pointer].isDirty = false;
				bm->numWrite++;
			}
			readBlock(pageNum, &fh, fList[bm->pointer].data);
			/* load the new frame from the file */
			fList[bm->pointer].pageNum = pageNum;
			fList[bm->pointer].fixCounts++;
			/* load the frame to pageHandle */
			bm->clock[bm->pointer] = true;
			load_frame_to_page(fList, bm->pointer, page);
			bm->numRead++;
			closePageFile(&fh);
			break;
		}
		case RS_LFU:{
			int min_frequency;
			int min_position;
			int k;
/* Error control*/
			for(k = 0; k < bm->numPages; k++){
				if(fList[k].fixCounts == 0) break;
			}
			if(k == bm->numPages) return RC_BM_BUFFER_FULL;

			for(min_position = k, min_frequency = fList[k].frequency; k < bm->numPages; k++){
				if(fList[k].frequency < min_frequency){
					min_frequency = fList[k].frequency;
					min_position = k;
				}
			}// This loop finds the position of the frame with minimum access frequency
			/* if the frame is dirty, flush it first */
			if(fList[min_position].isDirty == true){
				writeBlock(fList[min_position].pageNum, &fh, fList[min_position].data);
				fList[min_position].isDirty = false;
				bm->numWrite++;
			}
			readBlock(pageNum, &fh, fList[min_position].data);
			/* load the new frame from the file */
			fList[min_position].pageNum = pageNum;
			fList[min_position].fixCounts++;
			fList[min_position].frequency = 1;
			/* load the frame to pageHandle */
			load_frame_to_page(fList, min_position, page);
			bm->numRead++;
			closePageFile(&fh);
			break;
		} // Read block from min_position and load frame to page, update numRead.
  	}   
	
  }
    
  return RC_OK; 

}
