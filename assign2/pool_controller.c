#include "buffer_mgr.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>




// Buffer Manager Interface Pool Handling,

RC initBufferPool(BM_BufferPool *const bm, const char *const pageFileName, const int numPages, ReplacementStrategy strategy, void *stratData){
/*         Variables            */
	BM_Frame *fList;
	int i;
/* allocate memory for pageFileName  */
	bm->pageFile = (char*)malloc(strlen(pageFileName));
	strcpy(bm->pageFile, pageFileName);


	bm->numPages = numPages;
	bm->strategy = strategy;
	bm->numRead = 0;
	bm->numWrite = 0;
	bm->fl = list_new();
	bm->ll = list_new();
	bm->clock = (bool*)malloc(numPages * sizeof(bool));
	for(i = 0; i < numPages; i++){  //this loop sets clock to false
		bm->clock[i] = false;
	}
        bm->pointer = 0;
/*allocate memory for Frames and initialize with default values*/

	fList = (BM_Frame*)malloc(sizeof(BM_Frame) * numPages);
/*  initialize with default value */
	for(i = 0; i < numPages; i++){
		fList[i].isDirty = false;
		fList[i].fixCounts = 0;
                fList[i].pageNum = -1;
		fList[i].data = MAKE_PAGE_FRAME();
		fList[i].frequency = 0;
	}

	bm->mgmtData = fList;
	
	return RC_OK;
}


RC shutdownBufferPool(BM_BufferPool *const bm){
/*       Variables          */
	BM_Frame *fList;
	int i;
/* Get frame info                 */
	fList = (BM_Frame*)(bm->mgmtData);

	for(i = 0; i < bm->numPages; i++){
		if(fList[i].fixCounts > 0) return RC_BM_DESTROY_PINNED_PAGE;	
	}// if Frame fixCount >0, return RC_BM_DESTROY_PINNED_PAGE
/*           Error control       */
	if(forceFlushPool(bm) == RC_WRITE_FAILED){
		return RC_BM_WRITE_FAILED_DURING_FLUSH;
	}

/*       Free memory      */
	for(i = 0; i < bm->numPages; i++){
		free(fList[i].data);
	}
	free(fList);
	
	list_free(bm->fl);
	list_free(bm->ll);

	return RC_OK;
}


RC forceFlushPool(BM_BufferPool *const bm){
/*      Variables       */
	int i;
	int output_fd;
	int output_ret;
	BM_Frame* fList;
/*  get the frame info    */
	fList = (BM_Frame*)(bm->mgmtData);


	for(i = 0; i < bm->numPages; i++){

		if(fList[i].isDirty == true && fList[i].fixCounts == 0){
			output_fd = open(bm->pageFile, O_WRONLY); // create a file named with PageFile
			if(output_fd == -1) return RC_FILE_NOT_FOUND; /*output_fd == -1 only when creation fails (e.g., disk full, file already exists)*/
			output_ret = pwrite(output_fd, fList[i].data, PAGE_SIZE, fList[i].pageNum * PAGE_SIZE + PAGE_SIZE); // write the bytes in the frame to the file. 'write' function returns the number of bytes that are written
			if(output_ret != PAGE_SIZE) return RC_WRITE_FAILED; // Error control
			bm->numWrite++;
		}
		fList[i].isDirty = false;
	}// Check for fixCount, if fixCount equals to 0, write to the disk from buffer pool
	
	return RC_OK;
}


