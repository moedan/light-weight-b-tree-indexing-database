#include "storage_mgr.h"
#include "general_handler.h"


/* manipulating page files */





RC createPageFile (char *fileName){

/*		Variables		*/

  	int output_fd;		/*output file descriptor*/
  	int ret_out;		/*# of written bytes*/
	int i;
  	SM_FileHandle fHandle;

	char page[PAGE_SIZE];

/*					*/

	fHandle.totalNumPages = 1;
	fHandle.curPagePos = 0;		// initialize the variables

	
	
	for(i = 0; i < PAGE_SIZE; i++){
		/*
		This for loop just sets all bytes in the data field of the page to '\0'
		*/
		page[i] = '\0';
	}
	memcpy(page, &fHandle, sizeof(SM_FileHandle));		// copy the values of fHandle to the temporary buffer 'page'. Note that only the 'fileName' and 'mgmtInfo' fields have trash values currently. We only copy the values and do not use them in this function. This is for simplicity (so that we can read one struct instead of two integers later from the file header).
	
	unlink(fileName);	
  	output_fd = open(fileName, O_WRONLY | O_CREAT, S_IRWXU); // create a file named with fileName in the disk, S_IRWXU gives all privileges to the user. For details, type 'man 2 open' command in the terminal. Returns a posix file descriptor
	

  	if(output_fd == -1){
		/*
		output_fd == -1 only when creation fails (e.g., disk full, file already exists)
		*/
		close(output_fd);
  		return RC_WRITE_FAILED;
  	}

	ret_out = pwrite(output_fd, &page, PAGE_SIZE, 0); // write the bytes in fHandle to the file. 'write' function returns the number of bytes that are written


	if(ret_out != PAGE_SIZE){
		close(output_fd);
		return RC_WRITE_FAILED;

		/*
			if the written bytes are not equal to PAGE_SIZE, that means some error occured during the 'write' function and the function aborted
		*/
	}

	for(i = 0; i < sizeof(SM_FileHandle); i++){
		page[i] = '\0';
	}	// write 0 bytes to the temporary buffer 'page' since we used it to temporarily store 'fHandle' before
	
  	ret_out = pwrite(output_fd, &page, PAGE_SIZE, PAGE_SIZE);	// write the bytes of in the buffer 'page' to the file.

  	if(ret_out != PAGE_SIZE){	// if not all PAGE_SIZE bytes are written
		close(output_fd);
    		return RC_WRITE_FAILED;
  	}


  	close(output_fd); // close the file descriptor
  	return RC_OK;
}

RC openPageFile (char *fileName, SM_FileHandle *fHandle){
	
/*		Variables		*/

	int ret_in;	/*# of read bytes*/
	int input_fd;	/*input file descriptor*/
	int i;
	char buffer[PAGE_SIZE];	/*buffer to read bytes from the file*/
	SM_FileHandle temp;
	SM_BookKeepingInfo *iHandle;


/*		Error Control		*/
	
	if(fHandle == NULL) return RC_FILE_HANDLE_NOT_INIT;

/*					*/
	
	fHandle->fileName = (char*)malloc(strlen(fileName));	//	initialize the fileName since it is not initialized yet
	strcpy(fHandle->fileName, fileName);	
	
	input_fd = open(fileName, O_RDWR);	// open file to get file descriptor. we set O_RDWR (read and write) since this file may be read and written later


	if(input_fd == -1){	// input_fd == -1 only when the file is not found
		close(input_fd);
		return RC_FILE_NOT_FOUND;
	}

/*		Book Keeping Info. Initialization		*/

	iHandle = (SM_BookKeepingInfo*)malloc(sizeof(SM_BookKeepingInfo));	
	fHandle->mgmtInfo = iHandle;						
	iHandle->fileHandle = input_fd;	

/*								*/

	ret_in = pread(input_fd, &buffer, PAGE_SIZE, 0);	// read sizeof(SM_FileHandle) bytes from the file and store the bytes in buffer, which should be the SM_FileHandle of the file

	memcpy(&temp, buffer, sizeof(SM_FileHandle));	//copy the first sizeof(SM_FileHandle) portion to the temporary buffer 'buffer', which should be the SM_FileHandle

	fHandle->totalNumPages = temp.totalNumPages;	// copy the values to
	fHandle->curPagePos = temp.curPagePos;		// the fHandle







/*									*/
	
  	return RC_OK;
}

RC closePageFile (SM_FileHandle *fHandle){

/*		Variables		*/
	int fd;

	SM_BookKeepingInfo* iHandle;


/*		Error Control		*/

	if(fHandle == NULL) return RC_FILE_HANDLE_NOT_INIT;

/*					*/


	iHandle = (SM_BookKeepingInfo*) fHandle->mgmtInfo;
	
	fd = iHandle->fileHandle;

	pwrite(fd, fHandle, sizeof(SM_FileHandle), 0);

	

	
	free(iHandle);			// free the mgmtInfo since no longer necessary

	close(fd);	// close the system file handle
	
	

  	return RC_OK;
}

RC destroyPageFile (char *fileName){

/*		Variables		*/

	int msg;


/*					*/

	msg = unlink(fileName);
	if(msg == 0) return RC_OK;	// if successfully removed
	else return RC_FILE_NOT_FOUND;	// if not removed for some reason
  	
}
