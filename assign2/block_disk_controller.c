#include "storage_mgr.h"
#include "general_handler.h"


   




/* reading blocks from disc */

RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage){


/*		Variables		*/ 
    SM_BookKeepingInfo* iHandle;
    int fd;
    int ret_in;

/*		Error Control		*/

    if(fHandle == NULL) return RC_FILE_HANDLE_NOT_INIT;
    if (pageNum > fHandle->totalNumPages - 1 || pageNum < 0) return RC_READ_NON_EXISTING_PAGE;

/*					*/

    iHandle = (SM_BookKeepingInfo*)fHandle->mgmtInfo;	// read the SM_BookKeepingInfo struct
  

    fd = iHandle->fileHandle;	//restore the posix file descriptor

    
    ret_in = pread(fd, memPage, PAGE_SIZE, (pageNum + 1)*PAGE_SIZE);	//read the corresponding block of the file. pageNum + 1 because the first block is for the file header
    if (ret_in <0) return RC_READ_FAILED;
    fHandle->curPagePos = pageNum;	//	move the current page position to the pageNum
    return RC_OK;
}

int getBlockPos (SM_FileHandle *fHandle){
  	return fHandle->curPagePos;
}

RC readFirstBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){

/*		Variables		*/
    SM_BookKeepingInfo* iHandle;
    int fd; 
    int ret_in;   

/*		Error Control		*/    

    if(fHandle == NULL) return RC_FILE_HANDLE_NOT_INIT;

/*					*/


    iHandle = (SM_BookKeepingInfo*)fHandle->mgmtInfo;
    fd = iHandle->fileHandle;		//restore the posix file descriptor
    
    ret_in = pread(fd, memPage, PAGE_SIZE, PAGE_SIZE);	// read the first block of the file. read from the second block, which is the first page of the data
    if (ret_in != PAGE_SIZE) return RC_READ_FAILED;
    

    fHandle->curPagePos = 0;	// reading the page # 0 (i.e. first page)
    return RC_OK;
}


RC readPreviousBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){

/*		Variables		*/

    SM_BookKeepingInfo* iHandle;
    int fd;
    int ret_in;

/*		Error Control		*/

    if(fHandle == NULL) return RC_FILE_HANDLE_NOT_INIT;
    if (fHandle->curPagePos <= 0)    return RC_READ_NON_EXISTING_PAGE;

/*					*/

    iHandle = (SM_BookKeepingInfo*)fHandle->mgmtInfo;
    fd =  iHandle->fileHandle;		//restore the posix file descriptor


    ret_in = pread(fd, memPage, PAGE_SIZE,PAGE_SIZE*(fHandle->curPagePos));	// we have 1 block of file header at the front (+1), but we're reading the previous block (-1), which lead to fHandle->curPagePos + 0
    if (ret_in != PAGE_SIZE) return RC_READ_FAILED;
    
    fHandle->curPagePos --;	//position moves to the previous one
    return RC_OK;
}


RC readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    
/*		Variables		*/
    SM_BookKeepingInfo* iHandle;
    int fd;
    int ret_in;

/*		Error Control		*/

    if(fHandle == NULL) return RC_FILE_HANDLE_NOT_INIT;
    
/*					*/
    iHandle = (SM_BookKeepingInfo*)fHandle->mgmtInfo;
    fd = iHandle->fileHandle;		//restore the posix file descriptor

    ret_in = pread(fd, memPage, PAGE_SIZE,PAGE_SIZE*(fHandle->curPagePos+1));
    if (ret_in != PAGE_SIZE) return RC_READ_FAILED;

    return RC_OK;
}

RC readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){

/*		Variables		*/    
    
    SM_BookKeepingInfo* iHandle;
    int fd;
    int ret_in;

/*		Error Control		*/

    if(fHandle == NULL) return RC_FILE_HANDLE_NOT_INIT;
    if (fHandle->curPagePos == fHandle->totalNumPages) return RC_READ_NON_EXISTING_PAGE;

/*					*/

    
    iHandle = (SM_BookKeepingInfo*)fHandle->mgmtInfo;
    fd = iHandle->fileHandle;
    
    
    ret_in = pread(fd, memPage, PAGE_SIZE,PAGE_SIZE*(fHandle->curPagePos+2));	// +1 since we are reading the next block, and we have one block of file haeder at the front (+1), which lead to +2 in total
    if (ret_in != PAGE_SIZE) return RC_READ_FAILED;
    
    fHandle->curPagePos++;

    return RC_OK;
}


RC readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){

/*		Variables		*/
  
    SM_BookKeepingInfo* iHandle;
    int fd;
    int ret_in;

/*		Error Control		*/

    if(fHandle == NULL) return RC_FILE_HANDLE_NOT_INIT;

/*					*/
    
    iHandle = (SM_BookKeepingInfo*)fHandle->mgmtInfo;
    fd = iHandle->fileHandle;		//restore the posix file descriptor
    
    ret_in = pread(fd, memPage, PAGE_SIZE,PAGE_SIZE*(fHandle->totalNumPages));	// totalNumPages - 1 is the position of the last page, but we have one block of file header at the front.
    if (ret_in != PAGE_SIZE) return RC_READ_FAILED;
    
    fHandle->curPagePos = fHandle->totalNumPages-1;
    return RC_OK;
}
