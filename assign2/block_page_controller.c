#include "storage_mgr.h"
#include "general_handler.h"



/* writing blocks to a page file */


RC writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage){

/*		Variables		*/

	int output_fd;
	int ret_out;
	SM_BookKeepingInfo* iHandle;

/*		Error Control		*/

	if(fHandle == NULL) return RC_FILE_HANDLE_NOT_INIT;
	ensureCapacity(pageNum + 1, fHandle);	
	iHandle = (SM_BookKeepingInfo*)fHandle->mgmtInfo;
	
	output_fd = iHandle->fileHandle;	// restore the posix file descriptor

	ret_out = pwrite(output_fd, memPage, PAGE_SIZE, (pageNum + 1) * PAGE_SIZE);	// write at the pageNum-th page
	
	fHandle->curPagePos = pageNum;	// move the position to pageNum

	if(ret_out < 0) return RC_WRITE_FAILED;
	return RC_OK;

}


RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){

/*		Variables		*/

	int output_fd;
	int ret_out;
	SM_BookKeepingInfo* iHandle;
	ensureCapacity(fHandle->curPagePos + 1, fHandle);
/*		Error Control		*/

	if(fHandle == NULL) return RC_FILE_HANDLE_NOT_INIT;

/*					*/
	
	iHandle = (SM_BookKeepingInfo*)fHandle->mgmtInfo;
	
	output_fd = iHandle->fileHandle;	// restore the posix file descriptor

	ret_out = pwrite(output_fd, memPage, PAGE_SIZE, (fHandle->curPagePos + 1) * PAGE_SIZE);	// write at the current page

	if(ret_out < 0) return RC_WRITE_FAILED;
	return RC_OK;
}


RC appendEmptyBlock (SM_FileHandle *fHandle){

/*		Variables		*/

	int output_fd;
	int ret_out;
	int i;
	char buffer[PAGE_SIZE];
	SM_BookKeepingInfo* iHandle;

/*		Error Control		*/

	if(fHandle == NULL) return RC_FILE_HANDLE_NOT_INIT;

/*					*/
	
	iHandle = (SM_BookKeepingInfo*)fHandle->mgmtInfo;
	
	output_fd = iHandle->fileHandle;	// restore the posix file descriptor

	for(i = 0; i < PAGE_SIZE; i++){
		buffer[i] = '\0';
	}

	

	ret_out = pwrite(output_fd, buffer, PAGE_SIZE, (fHandle->totalNumPages + 1) * PAGE_SIZE);	// write an empty page at the end
	fHandle->totalNumPages ++;	// add 1 to the totalNumPages
	if(ret_out < 0) return RC_WRITE_FAILED;
	return RC_OK;

}


RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle){

/*		Variables		*/

	int i;

/*		Error Control		*/

	if(fHandle == NULL) return RC_FILE_HANDLE_NOT_INIT;

/*					*/

	if(fHandle->totalNumPages < numberOfPages){
		for(i = 0; i < numberOfPages - fHandle->totalNumPages; i++){
			appendEmptyBlock(fHandle);
		}
	}
	return RC_OK;

}
