#ifndef STORAGE_MGR_H
#define STORAGE_MGR_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include "dberror.h"

/************************************************************
 *                    handle data structures                *
 ************************************************************/
typedef struct SM_FileHandle {
  	char *fileName;
  	int totalNumPages;
	int curPagePos;
	void *mgmtInfo;
} SM_FileHandle;

typedef struct SM_OpenFile{
	SM_FileHandle *fHandle;
	struct SM_OpenFile *prev;
	struct SM_OpenFile *next;
} SM_OpenFile;

typedef char* SM_PageHandle;

typedef struct SM_BookKeepingInfo {
	int fileHandle;	// store the file handle returned by open() function
	SM_PageHandle* pageList;	//an array to store the list of SM_PageHandle pointing to the data area of the pages. Not used in 1st assignment.
} SM_BookKeepingInfo;	//struct for the book keeping info. This will be stored in mgmtInfo field in SM_FileHandle to track the pages of a file.




/************************************************************
 *                    shared variables                      *
 ************************************************************/


extern SM_OpenFile* firstFile;
extern SM_OpenFile* lastFile;


/************************************************************
 *                    interface                             *
 ************************************************************/
/* manipulating page files */
extern void initStorageManager (void);
extern RC createPageFile (char *fileName);
extern RC openPageFile (char *fileName, SM_FileHandle *fHandle);
extern RC closePageFile (SM_FileHandle *fHandle);
extern RC destroyPageFile (char *fileName);

/* reading blocks from disc */
extern RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage);
extern int getBlockPos (SM_FileHandle *fHandle);
extern RC readFirstBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);
extern RC readPreviousBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);
extern RC readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);
extern RC readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);
extern RC readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);

/* writing blocks to a page file */
extern RC writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage);
extern RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);
extern RC appendEmptyBlock (SM_FileHandle *fHandle);
extern RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle);

#endif
