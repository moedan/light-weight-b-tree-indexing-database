#ifndef LIST_H
#define LIST_H

/* a linked list head file to fulfill FIFO and LRU Replacement strategy*/

typedef struct Node{
  int cache_index;
  struct Node* next;
}Node;


typedef struct List{
  struct Node* head;
  struct Node* tail;
}List;

List* list_new(void);
List* list_add_element(List *, const int);
List* list_remove_element_X(List *,const int);
List* list_remove_element(List *);
List* list_free(List* );

#endif
