#include <stdio.h>
#include <string.h>
#include "storage_mgr.h"
#include "buffer_mgr.h"
#include "record_mgr.h"

/**
 * get Record Size
 * @param Schema *: schema
 * @return int: the record size
 */
int getRecordSize (Schema *schema){
	int recordSize = 0;
	int i;
	
	for(i = 0; i < schema->numAttr; i++){
		switch(schema->dataTypes[i]){
		case DT_INT:
			recordSize += sizeof(int);
			break;
		case DT_STRING:
			recordSize += schema->typeLength[i];
			break;
		case DT_FLOAT:
			recordSize += sizeof(float);
			break;
		case DT_BOOL:
			recordSize += sizeof(bool);
			break;
		}		
	}	

	return recordSize;
}

/**
 * create schema
 * @param int: number of attributes
 * @param char**: list of attrbute names
 * @param DataType*: list of data types
 * @param int*: list of type lengths
 * @param int: key size
 * @param int*: list of keys
 * @return Schema*: schema pointer
 */
extern Schema *createSchema (int numAttr, char **attrNames, DataType *dataTypes, int *typeLength, int keySize, int *keys){

	Schema *schema = (Schema*)malloc(sizeof(Schema));

	int i;

	schema->numAttr = numAttr;
	schema->attrNames = (char**)malloc(numAttr * sizeof(char*));

	for(i = 0; i < numAttr; i++){
		schema->attrNames[i] = (char*)malloc(strlen(attrNames[i]) + 1);
		strcpy(schema->attrNames[i], attrNames[i]);
	}

	schema->dataTypes = (DataType*)malloc(numAttr * sizeof(DataType));
	for(i = 0; i<numAttr;i++){
		schema->dataTypes[i]=dataTypes[i];
	}
	
	schema->typeLength = (int*)malloc(numAttr * sizeof(int));
	for(i = 0; i<numAttr;i++){
		schema->typeLength[i]=typeLength[i];
	}
	
	schema->keySize = keySize;
	
	schema->keyAttrs = (int*)malloc(keySize * sizeof(int));
	for(i = 0; i<keySize;i++){
		schema->keyAttrs[i]=keys[i];
	}

	return schema;
}

/**
 * free schema
 * @param Schema*: schema pointer
  * @return RC: error control
 */
RC freeSchema (Schema *schema){
	
	int i;
	for(i = 0; i < schema->numAttr; i++){
		free(schema->attrNames[i]);
	}
	free(schema->attrNames);
	free(schema->dataTypes);
	free(schema->typeLength);
	free(schema->keyAttrs);

	return RC_OK;
}
