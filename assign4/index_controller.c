#include <stdio.h>
#include "btree_mgr.h"

// init and shutdown index manager
RC initIndexManager (void *mgmtData){

	return RC_OK;
}

RC shutdownIndexManager (){

	return RC_OK;
}

// create, destroy, open, and close an btree index

RC createBtree (char *idxId, DataType keyType, int n){
	
	SM_FileHandle fh;
	
	createPageFile(idxId);
	openPageFile(idxId, &fh);

	char* buffer = (char*)malloc(PAGE_SIZE);

	bool isNull = true;
	bool isLeaf = true;
	bool isRoot = true;
	
	int i;

	for(i = 0; i < PAGE_SIZE; i++) buffer[i] = 0;

	int tree_n = n;
	DataType tree_key = keyType;
	int numNodes = 0;
	int numEntries = 0;

	memcpy(buffer, &tree_n, sizeof(int));	// Page 0 of the tree file is to store various header information
	memcpy(buffer + sizeof(int), &numNodes, sizeof(int));
	memcpy(buffer + 2*sizeof(int), &numEntries, sizeof(int));
	memcpy(buffer + 3*sizeof(int), &tree_key, sizeof(DataType));
	

	writeBlock(0, &fh, buffer);
	

	for(i = 0; i < PAGE_SIZE; i++) buffer[i] = 0;

	memcpy(buffer, &isNull, sizeof(bool));
	memcpy(buffer + sizeof(bool), &isLeaf, sizeof(bool));
	buffer[2 * sizeof(bool)] = 1;
	
	writeBlock(1, &fh, buffer);


	free(buffer);
	closePageFile(&fh);		
	return RC_OK;
}


RC openBtree (BTreeHandle **tree, char *idxId){

	SM_FileHandle fh;
	BM_BufferPool *bm = MAKE_POOL();
	BM_PageHandle ph;

	BTreeHandle * th = (BTreeHandle*)malloc(sizeof(BTreeHandle));

	
	initBufferPool(bm, idxId, 100, RS_LRU, NULL);	
	pinPage(bm, &ph, 0);


	int tree_n;
	int numNodes;
	int numEntries;
	DataType tree_key;

	char* buffer = ph.data;

	memcpy(&tree_n, buffer, sizeof(int));
	memcpy(&numNodes, buffer + sizeof(int), sizeof(int));
	memcpy(&numEntries, buffer + 2*sizeof(int), sizeof(int));
	memcpy(&tree_key, buffer + 3 * sizeof(int), sizeof(DataType));


	th->idxId = (char*)malloc(strlen(idxId) + 1);
	strcpy(th->idxId, idxId);
	th->keyType = tree_key;
	th->n = tree_n;
	th->numNodes = numNodes;
	th->mgmtData = bm;
		

	unpinPage(bm, &ph);

	free(buffer);
	
	*tree = th;	
	
	return RC_OK;
}


RC closeBtree (BTreeHandle *tree){

	BM_BufferPool *bm = (BM_BufferPool*)tree->mgmtData;
	BM_PageHandle ph;
	
	pinPage(bm, &ph, 0);

	memcpy(ph.data + sizeof(int), &(tree->numNodes), sizeof(int));
	memcpy(ph.data + 2 * sizeof(int), &(tree->numEntries), sizeof(int));
	
	markDirty(bm, &ph);
	
	unpinPage(bm, &ph);


	forceFlushPool(bm);
	shutdownBufferPool(bm);
	
	return RC_OK;
}


RC deleteBtree (char *idxId){

	destroyPageFile(idxId);
	return RC_OK;
}
