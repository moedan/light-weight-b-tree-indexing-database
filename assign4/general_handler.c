#include "storage_mgr.h"
#include "general_handler.h"

/* Global variables shared with all .c files */

SM_OpenFile *firstFile;
SM_OpenFile *lastFile;


void initStorageManager (void){
	firstFile = NULL;
	lastFile = NULL;
}




void initBookKeepingInfo(SM_FileHandle* fHandle){

/*
	This function initializes required pages of the file. In details, it does the following things.
	1. generates and initializes SM_BookKeepinfInfo
	2. generates and initializes an array of SM_PageHandle inside the SM_BookKeepinfInfo, then allocate PAGE_SIZE bytes of memory to all pages
	3. let fHandle->mgmtInfo points to iHandle;
*/


	int i;
	SM_BookKeepingInfo *iHandle;	
	SM_PageHandle *pageList;	

	/* 1 start */
	iHandle = (SM_BookKeepingInfo*)malloc(sizeof(SM_BookKeepingInfo));
	/* 1 end */

	/* 2 start */
	pageList = (SM_PageHandle*)malloc(fHandle->totalNumPages * sizeof(SM_PageHandle));
	for(i = 0; i < fHandle->totalNumPages; i++){
		pageList[i] = (SM_PageHandle)malloc(PAGE_SIZE);
	}
	iHandle->pageList = pageList;
	/* 2 end */

	/* 3 start */
	fHandle->mgmtInfo = iHandle;
	/* 3 end */

	
	/*
		Since we want to keep all memory area allocated in this function, we do not free any of them
	*/
}




void pushOpenFile(SM_OpenFile *file){

	/*
		This functions adds the 'file' to the end of the linked list
		The first and last fHandles (fistFile & lastFile resp.) in the linked list are globally accessible variables. Other .c files can also access them. 
	*/


	if(firstFile == NULL){
		file->prev = NULL;
		file->next = NULL;
		firstFile = file;
		lastFile = file;
	}else{
		file->prev = lastFile;
		file->next = NULL;
		lastFile->next = file;
		lastFile = file;
	}
}

RC removeOpenFile(SM_OpenFile *curr){
	/*
		This function removes the file named with 'fileName', and does the following things.
		1. unlinks the 'curr' from the linked list, and connects the previous and next fHandles.
		2. resets obsolete variables and free obsolete memory
	*/


	RC error = RC_FILE_NOT_FOUND;
	if(curr == NULL){
		return error;
	}else{
		
		/* 1 start */
		if(firstFile->next == NULL && lastFile->prev == NULL){
			firstFile = NULL;
			lastFile = NULL;
		}else{
			if(curr->prev == NULL){
				firstFile = curr->next;
				firstFile->prev = NULL;
			}else{
				curr->prev->next = curr->next;
			}

			if(curr->next == NULL){
				lastFile = curr->prev;
				lastFile->next = NULL;
			}else{
				curr->next->prev = curr->prev;
			}
		
		}

		
		/* 1 end */
		

		/* 2 start */
		free(curr);
		/* 2 end */	
	}
	
	return RC_OK;

}



void free_mgmtInfo(SM_FileHandle *fHandle){
	int i;
	SM_BookKeepingInfo* iHandle = (SM_BookKeepingInfo*)(fHandle->mgmtInfo);

	free(iHandle->pageList);
	free(iHandle);
	fHandle->mgmtInfo = iHandle = NULL;
}

