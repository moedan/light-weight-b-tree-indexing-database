#include <stdio.h>

#include "btree_mgr.h"


RC getNumNodes (BTreeHandle *tree, int *result){

	int rc = tree->numNodes;
	*result = rc;


	return RC_OK;
}

RC getNumEntries (BTreeHandle *tree, int *result){

	int rc = tree->numEntries;
	*result = rc;

	return RC_OK;
}


RC getKeyType (BTreeHandle *tree, DataType *result){

	*result = tree->keyType;

	return RC_OK;
}


