Manual:
	1. open the terminal
	2. use command 'cd' to get into the program folder 
	3. type 'make clean' to erase potentially existing redundant files
	4. type 'make' to compile
	5. type './test_assign3_1' to see the results

Notes:
	1.As the size of record is fixed, we can determine the numbers each page can store(#num) when the table is created. 
		And each page is written in the form of isFilled*#num + record-data*#num.
		isFilled is an indicator whether the slot has record or not.
	2.The program doesn't assign record id to a record when it is created, but at the time when it is inserted to table (first finds the empty page,slot and assigns it).
	3.delete record won't wipe the slot data, just simply mark the isFilled indicator as false.
	4.only the type length of string is given by user, all other 3 types(float, int, bool) is pre-defined by system.
		Please assign them to 0 when using. 
		
Main structure:
  DataType: enum structure to represent data types including int, float, string and bool.
	Value: record value, including data type and value
	RID: record id, including page number and slot number
	Schema: the record schema:
														number of attributes
														list of attribute names
														list of data types
														list of typeslengths
														list of key attributes
														key size
	RM_TableData: represent a table
																 table name
																 schema
																 page number for store schema
																 number of records per page
																 pointer to buffer manager

Our module mainly consists of five parts:(in four files: schema_controller.c, table_controller.c,record_controller.c,table_scanner.c)
	1.table and manager
		initRecordManager : init record manager
		shutdownRecordManager: shut down record manager
		createTable: create table
		openTable: open table
		closeTable: close table
		deleteTable: delete table
		getNumTuples: get number of tuples
		
	2.handling records in a table
		insertRecord: insert record
		deleteRecord: delete record
		updateRecord: update record
		getRecord: get record
   
	3.scans
		startScan: start scan
		next:
		closeScan: close scan

	4.dealing with schemas
		getRecordSize: get record size
		createSchema: create schema
		freeSchema: free schema

	5.dealing with records and attribute values
		createRecord: create record
		freeRecord: free record
		getAttr: get attribute
		setAttr: set attribute
