#include <stdio.h>
#include <string.h>
#include "btree_mgr.h"
// index access

#define OFFSET 1 // the header for the index file is located at the page 0, so we have an offset 1.
#define EON 1 // the character for End of Node

void nonLeafSplit(BTreeHandle *tree, int current_node);
void leafSplit(BTreeHandle *tree, int current_node);
void nonLeafUnderFlow(BTreeHandle *tree, int current_node);
void leafUnderFlow(BTreeHandle *tree, int current_node);


int advanceToXthKey(char* buffer, int x){

	int pos;
	int frequency = 0;
	
	for(pos = 0; buffer[pos] != EON; pos++){
		if(buffer[pos] == 'i') frequency++;
		if(frequency == x) break;
	}

	if(buffer[pos] != EON) return pos;
	else return -1;

}

int advanceToXthPtr(char* buffer, int x){

	int pos;
	int frequency = 0;
	
	for(pos = 0; buffer[pos] != EON; pos++){
		if(buffer[pos] == 'f') frequency++;
		if(frequency == x) break;
	}

	if(buffer[pos] != EON) return pos;
	else return -1;


}

void initBuffer(char* buf){
	int i;
	for(i = 0; i < PAGE_SIZE; i++){
		buf[i] = 0;
	}
}
void leafSplit(BTreeHandle *tree, int current_node){

	BM_BufferPool *bm = (BM_BufferPool*)tree->mgmtData;
	BM_PageHandle ph;

	
	int *key_pos = (int*)malloc((tree->n + 1) * sizeof(int));


	pinPage(bm, &ph, current_node + OFFSET);

	char* temp;

	int parent_node;

	int start, end, k;
	char* buffer = (char*)malloc(PAGE_SIZE);

	Value *val;
	
	end = 0;
	start = 0;

	if(current_node != 0){ // if the current node is not the root
		memcpy(&parent_node, ph.data + 2 * sizeof(bool), sizeof(int));
		temp = ph.data + 2 * sizeof(bool) + sizeof(int);
	}else{
		temp = ph.data + 2 * sizeof(bool);
	}

	for(end = 0, k = 0; temp[end] != EON; end++){
		if(temp[end] == 'i'){
			key_pos[k] = end;
			k++;	
		}
	}
	if (k <= tree->n){
		free(key_pos);
		return;
	}
	else{
		if(current_node == 0){ // it is the root node needs to be splitted
			int middle = (tree->n + 1) / 2;
			char *left, *right, *parent;
			left = (char*)malloc(PAGE_SIZE);
			right = (char*)malloc(PAGE_SIZE);
			parent = (char*)malloc(PAGE_SIZE);

			float left_most, left_middle, right_middle;

			initBuffer(left);
			initBuffer(right);
			initBuffer(parent);


			int pos;
			int new_left = tree->numNodes;
			int new_right = tree->numNodes + 1;

			pos = advanceToXthPtr(temp, middle + 2);
			strcpy(right, temp + pos);
			temp[pos] = 0;
			pos = advanceToXthKey(right, 1);
			right[pos] = 0;
			val = stringToValue(right);
			right_middle = val->v.floatV;
			free(val);
			right[pos] = 'i';
			//Extract the right child from the full root, 
			//and cache the right_middle pointer
			
			
			pos = advanceToXthKey(right, 1);
			int temp_pos = advanceToXthPtr(right, 2);
			if(temp_pos < 0) temp_pos = strlen(right) - 1;
			char temp_char = right[temp_pos];
			right[temp_pos] = 0;
			strcpy(parent, right + pos);
			right[temp_pos] = temp_char;
			

			//Extract the new root from the full root, 
			//and cache the right_middle pointer
			//after the cache, I only need the key values
			//so I'm setting the parent[pos]=0 for later use


			strcpy(left, temp);
			pos = advanceToXthPtr(left, middle + 1);
			temp_pos = advanceToXthKey(left, middle + 1);
			left[temp_pos] = 0;
			val = stringToValue(left + pos);
			left_middle = val->v.floatV;
			free(val);
			left[temp_pos] = 'i';
			pos = advanceToXthKey(left, 1);
			left[pos] = 0;
			val = stringToValue(left);
			left_most = val->v.floatV;
			left[pos] = 'i';
			free(val);
			//Extract the left child from the full root, 
			//and cache the left_most and left_middle pointer


			val = (Value*)malloc(sizeof(Value));
			val->dt = DT_FLOAT;
			val->v.floatV = (new_right) * 1.0;
			strcat(left, serializeValueModified(val));
			free(val);
			left[strlen(left)] = EON;
			//append the pointer to the new right child and the EON

			
			initBuffer(buffer);
			strcat(buffer, right);
			initBuffer(right);
			strcpy(right, buffer);
			//process for the right child. insert right_middle pointer at thefront.
			//no need to append EON since it's already in the right_child
			
			initBuffer(buffer);
			val = (Value*)malloc(sizeof(Value));
			val->dt = DT_FLOAT;
			val->v.floatV = (new_left) * 1.0;
			strcat(buffer, serializeValueModified(val));
			strcat(buffer, parent);
			val->v.floatV = (new_right) * 1.0;
			strcat(buffer, serializeValueModified(val));
			buffer[strlen(buffer)] = EON;
			initBuffer(parent);
			strcpy(parent, buffer);
			free(val);
			//process for the new root. insert two pointers to 
			//new left & right children, and append the EON.
			


			initBuffer(ph.data);

			bool isNull = false;
			bool isLeaf = false;

			memcpy(ph.data, &isNull, sizeof(bool));
			memcpy(ph.data + sizeof(bool), &isLeaf, sizeof(bool));
			temp = ph.data + 2*sizeof(bool);
			strcat(temp, parent); // copy the new root to the page file

//			printf("New root node %d: %s\n", ph.pageNum - 1, temp);

			markDirty(bm, &ph);
			unpinPage(bm, &ph);

			BM_PageHandle ph_left;
			BM_PageHandle ph_right;

						
			pinPage(bm, &ph_left, new_left + OFFSET);
			pinPage(bm, &ph_right, new_right + OFFSET);


			isLeaf = true;
			memcpy(ph_left.data, &isNull, sizeof(bool));
			memcpy(ph_left.data + sizeof(bool), &isLeaf , sizeof(bool));
			memcpy(ph_left.data + 2*sizeof(bool), &current_node, sizeof(int));
			strcat(ph_left.data + 2*sizeof(bool) + sizeof(int), left);
			
//			printf("New left node %d: %s\n", new_left, ph_left.data + 2*sizeof(bool) + sizeof(int));

			markDirty(bm, &ph_left);
			unpinPage(bm, &ph_left); // copy the new left node to the page file

			memcpy(ph_right.data, &isNull, sizeof(bool));
			memcpy(ph_right.data + sizeof(bool), &isLeaf , sizeof(bool));
			memcpy(ph_right.data + 2*sizeof(bool), &current_node, sizeof(int));
			strcat(ph_right.data + 2*sizeof(bool) + sizeof(int), right);

//			printf("New right node %d: %s\n", new_right, ph_right.data + 2*sizeof(bool) + sizeof(int));

			markDirty(bm, &ph_right);
			unpinPage(bm, &ph_right); // copy the new right node to the page file


			tree->numNodes = tree->numNodes + 2;
			
			free(left);
			free(right);
			free(parent);
			free(buffer);
		}else{ // it is a normal leaf node needs to be splitted
			int middle = (tree->n + 1) / 2;
			char *left, *right, *parent;
			left = (char*)malloc(PAGE_SIZE);
			right = (char*)malloc(PAGE_SIZE);
			parent = (char*)malloc(PAGE_SIZE);

			float left_most, left_middle, right_middle;

			initBuffer(left);
			initBuffer(right);
			initBuffer(parent);


			int pos;
			int new_left = current_node;
			int new_right = tree->numNodes;

			pos = advanceToXthPtr(temp, middle + 2);
			strcpy(right, temp + pos);
			temp[pos] = 0;
			pos = advanceToXthKey(right, 1);
			right[pos] = 0;
			val = stringToValue(right);
			right_middle = val->v.floatV;
			free(val);
			right[pos] = 'i';
			//Extract the right child from the full root, 
			//and cache the right_middle pointer
			
			
			pos = advanceToXthKey(right, 1);
			int temp_pos = advanceToXthPtr(right, 2);
			if(temp_pos < 0) temp_pos = strlen(right) - 1;
			char temp_char = right[temp_pos];
			right[temp_pos] = 0;
			strcpy(parent, right + pos);
			right[temp_pos] = temp_char;
			val = stringToValue(parent);
			int key = val->v.intV;
			free(val);
			

			//Extract the new root from the full root, 
			//and cache the right_middle pointer
			//after the cache, I only need the key values
			//so I'm setting the parent[pos]=0 for later use


			strcpy(left, temp);
			pos = advanceToXthPtr(left, middle + 1);
			temp_pos = advanceToXthKey(left, middle + 1);
			left[temp_pos] = 0;
			val = stringToValue(left + pos);
			left_middle = val->v.floatV;
			free(val);
			left[temp_pos] = 'i';
			pos = advanceToXthKey(left, 1);
			left[pos] = 0;
			val = stringToValue(left);
			left_most = val->v.floatV;
			left[pos] = 'i';
			free(val);
			//Extract the left child from the full root, 
			//and cache the left_most and left_middle pointer


			val = (Value*)malloc(sizeof(Value));
			val->dt = DT_FLOAT;
			val->v.floatV = (new_right) * 1.0;
			strcat(left, serializeValueModified(val));
			free(val);
			left[strlen(left)] = EON;
			//append the pointer to the new right child and the EON

			
			initBuffer(buffer);
			strcat(buffer, right);
			initBuffer(right);
			strcpy(right, buffer);
			//process for the right child. insert right_middle pointer at thefront.
			//no need to append EON since it's already in the right_child
			
			initBuffer(buffer);
			val = (Value*)malloc(sizeof(Value));
			val->dt = DT_FLOAT;
			val->v.floatV = (new_left) * 1.0;
			strcat(buffer, serializeValueModified(val));
			strcat(buffer, parent);
			val->v.floatV = (new_right) * 1.0;
			strcat(buffer, serializeValueModified(val));
			buffer[strlen(buffer)] = EON;
			initBuffer(parent);
			strcpy(parent, buffer);
			free(val);
			//process for the new root. insert two pointers to 
			//new left & right children, and append the EON.
	




			initBuffer(ph.data);
			bool isNull = false;
			bool isLeaf = true;
			memcpy(ph.data, &isNull, sizeof(bool));
			memcpy(ph.data + sizeof(bool), &isLeaf, sizeof(bool));
			memcpy(ph.data + 2 * sizeof(bool), &parent_node, sizeof(int));
			strcpy(ph.data + 2 * sizeof(bool) + sizeof(int), left);

//			printf("Existing left node %d: %s\n", ph.pageNum -1, ph.data + 2*sizeof(bool) + sizeof(int));

			markDirty(bm, &ph);
			unpinPage(bm, &ph);

			
			pinPage(bm, &ph, new_right + OFFSET);
			
			memcpy(ph.data, &isNull, sizeof(bool));
			memcpy(ph.data + sizeof(bool), &isLeaf, sizeof(bool));
			memcpy(ph.data + 2*sizeof(bool), &parent_node, sizeof(int));
			strcpy(ph.data + 2*sizeof(bool) + sizeof(int), right);

//			printf("New right node %d: %s\n", ph.pageNum - 1, ph.data + 2 * sizeof(bool) + sizeof(int));

			markDirty(bm, &ph);
			unpinPage(bm, &ph);

			// store the left child (unchanged) and right child (new node)


			pinPage(bm, &ph, parent_node + OFFSET);
			
			temp = ph.data + 2*sizeof(bool);

			if(parent_node != 0) temp = ph.data + 2 * sizeof(bool) + sizeof(int);
			
			initBuffer(buffer);
			
			int i;
			for(i = 0; i < tree->n; i++){
				pos = advanceToXthKey(temp, i + 1);
				if(pos < 0) break;
				strcpy(buffer, temp + pos);
				buffer[advanceToXthPtr(buffer, 1)] = 0;
				val = stringToValue(buffer);
				if(key < val->v.intV){
					free(val);
					break; // we have found a spot to insert
				}
				free(val);
			}
			
			if(pos < 0 || i == tree->n){
				pos = strlen(temp) - 1;
			}
			
			initBuffer(left);
			initBuffer(right);
			initBuffer(buffer);
			strcpy(buffer, temp);
			buffer[strlen(buffer) - 1] = 0; 


			strcpy(right, buffer + pos); // get the right half
			buffer[pos] = 0;
			strcpy(left, buffer); // get the left half

			val = (Value*)malloc(sizeof(Value));
			val->dt = DT_INT;
			val->v.intV = key;
			strcat(left, serializeValueModified(val)); // append the key to the left half
			
			val->dt = DT_FLOAT;
			val->v.floatV = new_right * 1.0;
			strcat(left, serializeValueModified(val)); // append the ptr to the left half
			
			strcat(left, right); // append the right half to the left half
			left[strlen(left)] = EON; // set the EON
			left[strlen(left)] = 0; // insertion done
			
			strcpy(temp, left);

//			printf("Merged parent node %d: %s\n", ph.pageNum - 1, temp);

			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			
			tree->numNodes = tree->numNodes + 1;


			free(left);
			free(right);
			free(parent);
			free(buffer);

			nonLeafSplit(tree, parent_node);

		} // if (root) ends
		
	}// if (overflows) ends
	free(key_pos);


}

void nonLeafSplit(BTreeHandle *tree, int current_node){
	BM_BufferPool *bm = (BM_BufferPool*)tree->mgmtData;
	BM_PageHandle ph;

	
	int *key_pos = (int*)malloc((tree->n + 1) * sizeof(int));


	pinPage(bm, &ph, current_node + OFFSET);

	char* temp;

	int parent_node;

	int start, end, k;
	char* buffer = (char*)malloc(PAGE_SIZE);

	Value *val;
	
	end = 0;
	start = 0;

	if(current_node != 0){ // if the current node is not the root
		memcpy(&parent_node, ph.data + 2 * sizeof(bool), sizeof(int));
		temp = ph.data + 2 * sizeof(bool) + sizeof(int);
	}else{
		temp = ph.data + 2 * sizeof(bool);
	}

	for(end = 0, k = 0; temp[end] != EON; end++){
		if(temp[end] == 'i'){
			key_pos[k] = end;
			k++;	
		}
	}
	if (k <= tree->n){ // if the nodes does not overflow
		free(key_pos);
		return;
	}else{ // if the node does overflow
		if(current_node == 0){ // if the node is a root


			int middle = (tree->n + 1) / 2;
			char *left, *right, *parent;
			left = (char*)malloc(PAGE_SIZE);
			right = (char*)malloc(PAGE_SIZE);
			parent = (char*)malloc(PAGE_SIZE);

			float left_most, left_middle, right_middle, right_most;

			initBuffer(left);
			initBuffer(right);
			initBuffer(parent);


			int pos;
			int new_left = tree->numNodes;
			int new_right = tree->numNodes + 1;

			pos = advanceToXthPtr(temp, middle + 2);
			strcpy(right, temp + pos);
			temp[pos] = 0;
			pos = advanceToXthKey(right, 1);
			right[pos] = 0;
			val = stringToValue(right);
			right_middle = val->v.floatV;
			free(val);
			right[pos] = 'i';

			pos = advanceToXthPtr(right, 1);
			right[strlen(right) - 1] = 0;
			val = stringToValue(right + pos);
			right_most = val->v.floatV;
			free(val);
			right[strlen(right)] = EON;

			//Extract the right child from the full root, 
			//and cache the right_middle and right_most pointer
			

			strcpy(parent, temp + key_pos[middle]);
			temp[key_pos[middle]] = 0;
			//Extract the new root from the full root, 
			//and cache the right_middle pointer
			//after the cache, I only need the key values
			//so I'm setting the parent[pos]=0 for later use


			strcpy(left, temp);
			pos = advanceToXthPtr(left, middle + 1);
			val = stringToValue(left + pos);
			left_middle = val->v.floatV;
			free(val);
			pos = advanceToXthKey(left, 1);
			left[pos] = 0;
			val = stringToValue(left);
			left_most = val->v.floatV;
			left[pos] = 'i';
			free(val);
			//Extract the left child from the full root, 
			//and cache the left_most and left_middle pointer


			strcat(left, parent);
			val = (Value*)malloc(sizeof(Value));
			val->dt = DT_FLOAT;
			val->v.floatV = (new_right) * 1.0;
			strcat(left, serializeValueModified(val));
			free(val);
			//After all the extraction, append the middle key to the left child
			//and append the pointer to the new right child
			//and append the EON

	

			left[strlen(left)] = EON;
			//proess for the left child. appending the EON

			
			initBuffer(buffer);
			strcat(buffer, right);
			initBuffer(right);
			strcpy(right, buffer);
			//process for the right child. insert right_middle pointer at thefront.
			//no need to append EON since it's already in the right_child
			
			initBuffer(buffer);
			val = (Value*)malloc(sizeof(Value));
			val->dt = DT_FLOAT;
			val->v.floatV = (new_left) * 1.0;
			strcat(buffer, serializeValueModified(val));
			strcat(buffer, parent);
			val->v.floatV = (new_right) * 1.0;
			strcat(buffer, serializeValueModified(val));
			buffer[strlen(buffer)] = EON;
			initBuffer(parent);
			strcpy(parent, buffer);
			free(val);
			//process for the new root. insert two pointers to 
			//new left & right children, and append the EON.



			initBuffer(ph.data);

			bool isNull = false;
			bool isLeaf = false;

			memcpy(ph.data, &isNull, sizeof(bool));
			memcpy(ph.data + sizeof(bool), &isLeaf, sizeof(bool));
			temp = ph.data + 2*sizeof(bool);
			strcat(temp, parent);

			markDirty(bm, &ph);
			unpinPage(bm, &ph);

			BM_PageHandle ph_left;
			BM_PageHandle ph_right;


			pinPage(bm, &ph_left, new_left + OFFSET);
			pinPage(bm, &ph_right, new_right + OFFSET);


			memcpy(ph_left.data, &isNull, sizeof(bool));
			memcpy(ph_left.data + sizeof(bool), &isLeaf , sizeof(bool));
			memcpy(ph_left.data + 2*sizeof(bool), &current_node, sizeof(int));
			strcat(ph_left.data + 2*sizeof(bool) + sizeof(int), left);
			markDirty(bm, &ph_left);
			unpinPage(bm, &ph_left);
			// copy the left node to the page file

			memcpy(ph_right.data, &isNull, sizeof(bool));
			memcpy(ph_right.data + sizeof(bool), &isLeaf , sizeof(bool));
			memcpy(ph_right.data + 2*sizeof(bool), &current_node, sizeof(int));
			strcat(ph_right.data + 2*sizeof(bool) + sizeof(int), right);
			markDirty(bm, &ph_right);
			unpinPage(bm, &ph_right);
			// copy the right node to the page file

			
			pinPage(bm, &ph, (int)(left_most + 0.5) + OFFSET);
			memcpy(ph.data + 2*sizeof(bool),  &new_left, sizeof(int));
			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			// update the parent node of the left most child

			pinPage(bm, &ph, (int)(left_middle + 0.5) + OFFSET);
			memcpy(ph.data + 2*sizeof(bool),  &new_left, sizeof(int));
			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			// update the parent node of the left middle child

			pinPage(bm, &ph, (int)(right_middle + 0.5) + OFFSET);
			memcpy(ph.data + 2*sizeof(bool),  &new_right, sizeof(int));
			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			// update the parent node of the right middle child

			pinPage(bm, &ph, (int)(right_most + 0.5) + OFFSET);
			memcpy(ph.data + 2*sizeof(bool),  &new_right, sizeof(int));
			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			// update the parent node of the right most child

//			printf("Node split at non-leaf root, numNodes + 2\n");

			tree->numNodes = tree->numNodes + 2;
			
			free(left);
			free(right);
			free(parent);
			free(buffer);

		}else{ // if the node is not a root



			int middle = (tree->n + 1) / 2;
			char *left, *right, *parent;
			left = (char*)malloc(PAGE_SIZE);
			right = (char*)malloc(PAGE_SIZE);
			parent = (char*)malloc(PAGE_SIZE);

			float left_most, left_middle, right_middle, right_most;

			int new_left = current_node;
			int new_right = tree->numNodes; // the new node

			initBuffer(left);
			initBuffer(right);
			initBuffer(parent);


			int pos;

			pos = advanceToXthPtr(temp, middle + 2);
			strcpy(right, temp + pos);
			temp[pos] = 0;
			pos = advanceToXthKey(right, 1);
			right[pos] = 0;
			val = stringToValue(right);
			right_middle = val->v.floatV;
			free(val);
			right[pos] = 'i';
			//Extract the right child from the full root, 
			//and cache the right_most pointer
			

			strcpy(parent, temp + key_pos[middle]);
			val = stringToValue(parent);
			int key = val->v.intV;
//			printf("middle key to be inserted is %d \n", key);
			temp[key_pos[middle]] = 0;
			//Extract the new root from the full root, 
			//and cache the right_middle pointer
			//after the cache, I only need the key values
			//so I'm setting the parent[pos]=0 for later use


			strcpy(left, temp);
			pos = advanceToXthPtr(left, middle + 1);
			val = stringToValue(left + pos);
			left_middle = val->v.floatV;
			free(val);
			pos = advanceToXthKey(left, 1);
			left[pos] = 0;
			val = stringToValue(left);
			left_most = val->v.floatV;
			left[pos] = 'i';
			free(val);

			pos = advanceToXthPtr(right, 1);
			right[strlen(right) - 1] = 0;
			val = stringToValue(right + pos);
			right_most = val->v.floatV;
			free(val);
			right[strlen(right)] = EON;

			//Extract the left child from the full root, 
			//and cache the left_most and left_middle pointer


			strcat(left, parent);
			val = (Value*)malloc(sizeof(Value));
			val->dt = DT_FLOAT;
			val->v.floatV = (new_right) * 1.0;
			strcat(left, serializeValueModified(val));
			free(val);
			//After all the extraction, append the middle key to the left child
			//and append the pointer to the new right child
			//and append the EON

	

			left[strlen(left)] = EON;
			//proess for the left child. appending the EON

			
			initBuffer(buffer);
			strcat(buffer, right);
			initBuffer(right);
			strcpy(right, buffer);
			//process for the right child. insert right_middle pointer at thefront.
			//no need to append EON since it's already in the right_child






			initBuffer(ph.data);

			bool isNull = false;
			bool isLeaf = false;

			memcpy(ph.data, &isNull, sizeof(bool));
			memcpy(ph.data + sizeof(bool), &isLeaf, sizeof(bool));
			memcpy(ph.data + 2 * sizeof(bool), &parent_node, sizeof(int));
			strcpy(ph.data + 2 * sizeof(bool) + sizeof(int), left);
			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			//copy the left node to the page file


			

			pinPage(bm, &ph, (int)(left_most + 0.5) + OFFSET);
			memcpy(ph.data + 2*sizeof(bool), &new_left, sizeof(int));
			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			// update the parent node of the left most child

			pinPage(bm, &ph, (int)(left_middle + 0.5) + OFFSET);
			memcpy(ph.data + 2*sizeof(bool), &new_left, sizeof(int));
			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			// update the parent node of the left middle child

			pinPage(bm, &ph, (int)(right_middle + 0.5) + OFFSET);
			memcpy(ph.data + 2*sizeof(bool), &new_right, sizeof(int));
			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			// update the parent node of the right middle child

			pinPage(bm, &ph, (int)(right_most + 0.5) + OFFSET);
			memcpy(ph.data + 2*sizeof(bool), &new_right, sizeof(int));
			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			// update the parent node of the right most child



	
			pinPage(bm, &ph, new_right + OFFSET);
			
			memcpy(ph.data, &isNull, sizeof(bool));
			memcpy(ph.data + sizeof(bool), &isLeaf, sizeof(bool));
			memcpy(ph.data + 2*sizeof(bool), &parent_node, sizeof(int));
			strcpy(ph.data + 2*sizeof(bool) + sizeof(int), right);
			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			// copy the right node to the page file

			pinPage(bm, &ph, parent_node + OFFSET);
			
			temp = ph.data + 2*sizeof(bool);
			if(parent_node != 0){ // if the parent node is not a root
				temp = ph.data + 2*sizeof(bool) + sizeof(int);
			}
			initBuffer(buffer);
			
			int i;
			for(i = 0; i < tree->n; i++){
				pos = advanceToXthKey(temp, i + 1);
				if(pos < 0) break;
				strcpy(buffer, temp + pos);
				buffer[advanceToXthPtr(buffer, 1)] = 0;
				val = stringToValue(buffer);
				if(key < val->v.intV) break; // we have found a spot to insert
				free(val);
			}
			if(pos < 0 || i == tree->n){
				pos = strlen(temp) - 1;
			}
			// in what follows, we partition the node into halves
			
			initBuffer(left);
			initBuffer(right);
			initBuffer(buffer);
			strcpy(buffer, temp);
			buffer[strlen(buffer) - 1] = 0;
			strcpy(right, buffer + pos); // get the right half

			buffer[pos] = 0;
			strcpy(left, buffer); // get the left half

			val = (Value*)malloc(sizeof(Value));
			val->dt = DT_INT;
			val->v.intV = key;
			strcat(left, serializeValueModified(val)); // append the key
			
			val->dt = DT_FLOAT;
			val->v.floatV = new_right * 1.0;
			strcat(left, serializeValueModified(val)); // append the ptr to the new node
			
			strcat(left, right); // append the right half to the left half
			left[strlen(left)] = EON;
			left[strlen(left)] = 0;
			// insertion done
			
			strcpy(temp, left);
			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			// copy the inserted node to the page file
			
//			printf("Node split at non-leaf non-root, numNodes + 1\n");

			tree->numNodes = tree->numNodes + 1;


			free(left);
			free(right);
			free(parent);
			free(buffer);

			nonLeafSplit(tree, parent_node);


		} // if (root) ends

	} // if(overflow) ends
	free(key_pos);
}


void leafUnderFlow(BTreeHandle *tree, int current_node){

	BM_BufferPool *bm = (BM_BufferPool*)tree->mgmtData;
	BM_PageHandle ph;
	
	int *key_pos = (int*)malloc((tree->n + 1) * sizeof(int));

	pinPage(bm, &ph, current_node + OFFSET);

	char* temp;

	int parent_node;
	int start, end, k;
	char* buffer = (char*)malloc(PAGE_SIZE);

	Value *val;
	
	end = 0;
	start = 0;

	if(current_node != 0){ // if the current node is not the root
		memcpy(&parent_node, ph.data + 2 * sizeof(bool), sizeof(int));
		temp = ph.data + 2 * sizeof(bool) + sizeof(int);
	}else{
		temp = ph.data + 2 * sizeof(bool);
	}

	for(end = 0, k = 0; temp[end] != EON; end++){
		if(temp[end] == 'i'){
			key_pos[k] = end;
			k++;	
		}
	}

	int min_key;
	min_key = (tree->n + 1) / 2;

	if (k >= min_key){ // if the node does not underflow
		free(key_pos);
		return;
	}else{ // if the node does underflow
		
		
	}// if(underflow) ends

}

void nonLeafUnderFlow(BTreeHandle *tree, int current_node){

	BM_BufferPool *bm = (BM_BufferPool*)tree->mgmtData;
	BM_PageHandle ph;
	
	int *key_pos = (int*)malloc((tree->n + 1) * sizeof(int));

	pinPage(bm, &ph, current_node + OFFSET);

	char* temp;

	int parent_node;
	int start, end, k;
	char* buffer = (char*)malloc(PAGE_SIZE);

	Value *val;
	
	end = 0;
	start = 0;

	if(current_node != 0){ // if the current node is not the root
		memcpy(&parent_node, ph.data + 2 * sizeof(bool), sizeof(int));
		temp = ph.data + 2 * sizeof(bool) + sizeof(int);
	}else{
		temp = ph.data + 2 * sizeof(bool);
	}

	for(end = 0, k = 0; temp[end] != EON; end++){
		if(temp[end] == 'i'){
			key_pos[k] = end;
			k++;	
		}
	}

	int min_key;
	min_key = ((tree->n + 2) / 2) - 1;
	

	if (k >= min_key){ // if the nodes does not overflow
		free(key_pos);
		return;
	}

}

RC findKey (BTreeHandle *tree, Value *key, RID *result){

	int root = 0;
	int current_node = root;

	int current_key;
	float prev_ptr;
	int parent_node;

	bool isNull;
	bool isLeaf;
	bool isRoot;

	BM_BufferPool *bm = (BM_BufferPool*)tree->mgmtData;
	
	BM_PageHandle ph;


	while(true){

		pinPage(bm, &ph, current_node + OFFSET);


		memcpy(&isNull, ph.data, sizeof(bool));
		memcpy(&isLeaf, ph.data + sizeof(bool), sizeof(bool));

		
		int start, end, k;	
		char* temp = ph.data + (2 * sizeof(bool));

		char* buffer = (char*)malloc(PAGE_SIZE);

		Value *val;
		
		end = 0;


		if(current_node != root){ // if the current node is not the root
			memcpy(&parent_node, ph.data + 2*sizeof(bool), sizeof(int));
			temp = temp + sizeof(int);
		}

		
		//printf("Finding key %d from current node %d: %s\n", key->v.intV, ph.pageNum -1, temp);


		start = 0;
		for(end = start, k = 0; temp[end] != EON; end++){
			if(temp[end] == 'i'){
				initBuffer(buffer);
				memcpy(buffer, temp + start, end - start);
				val = stringToValue(buffer);
				
				prev_ptr = val->v.floatV;
				start = end;
				free(val);
			}
			if(temp[end] == 'f'){
				initBuffer(buffer);
				memcpy(buffer, temp + start, end - start);
				val = stringToValue(buffer);
				
				current_key = val->v.intV;
				free(val);

				if(key->v.intV < current_key && isLeaf == false){
					current_node = (int)(prev_ptr + 0.5);
					break;
				} 
				if(key->v.intV <= current_key && isLeaf == true){
					break;
				}  
				start = end;
				
			}
		}
		

		if(temp[end] != EON && isLeaf == false){ // continue searching the target key
			unpinPage(bm, &ph);
			continue;
		}
		if(temp[end] != EON && isLeaf == true){
			
			if(key->v.intV < current_key) return RC_IM_KEY_NOT_FOUND;
			
			for(start = end - 1; start >= 0; start--){
				if(temp[start] == 'f') break;
				if(temp[start] == 'i') end = start;
			}
			temp[end] = 0;
			val = stringToValue(temp + start);
			temp[end] = 'i';
			int page, slot;
			page = (int)val->v.floatV;
			slot = (int)(((val->v.floatV) - page) * 10 + 0.5);
			result->page = page;
			result->slot = slot;			
			unpinPage(bm, &ph);
			break;
			
		}
		if(temp[end] == EON && isLeaf == false){ // continue searching the target key
			initBuffer(buffer);
			memcpy(buffer, temp + start, end - start);
			val = stringToValue(buffer);
			prev_ptr = val->v.floatV;
			current_node = (int)(prev_ptr + 0.5);
			free(val);
			unpinPage(bm, &ph);
			continue;
		}
		if(temp[end] == EON && isLeaf == true){
			if(temp[end] == EON){
				temp[end] = 0;
				val = stringToValue(temp + start);
				current_key = val->v.intV;
				temp[end] = EON;
				free(val);
				unpinPage(bm, &ph);
				if(key->v.intV != current_key){
					free(buffer);
					return RC_IM_KEY_NOT_FOUND;
				}
				else{
					for(start = end - 1; start >= 0; start--){
						if(temp[start] == 'f') break;
						if(temp[start] == 'i') end = start;
					}
					temp[end] = 0;
					val = stringToValue(temp + start);
					temp[end] = 'i';
					int page, slot;
					page = (int)val->v.floatV;
					slot = (int)(((val->v.floatV) - page) * 10 + 0.5);
					result->page = page;
					result->slot = slot;
					free(val);
					break;
				}
			
			}
		}

		free(buffer);

	}


	return RC_OK;
}



RC insertKey (BTreeHandle *tree, Value *key, RID rid){

	RID* temp_RID = (RID*)malloc(sizeof(RID));

	if(findKey(tree, key, temp_RID) != RC_IM_KEY_NOT_FOUND) return RC_IM_KEY_ALREADY_EXISTS;

	free(temp_RID);

	int root = 0;
	int current_node = root;

	int current_key;
	float prev_ptr;
	int parent_node;

	bool isNull;
	bool isLeaf;
	bool isRoot;

	BM_BufferPool *bm = (BM_BufferPool*)tree->mgmtData;
	
	BM_PageHandle ph;


	while(true){

		pinPage(bm, &ph, current_node + OFFSET);
		memcpy(&isNull, ph.data, sizeof(bool));
		memcpy(&isLeaf, ph.data + sizeof(bool), sizeof(bool));

		
		int start, end, k;	
		char* temp = ph.data + (2 * sizeof(bool));

		char* buffer = (char*)malloc(PAGE_SIZE);

		Value *val;
		
		end = 0;


		if(current_node != root){ // if the current node is not the root
			memcpy(&parent_node, ph.data + 2*sizeof(bool), sizeof(int));
			temp = temp + sizeof(int);
		}

		
		start = 0;
		for(end = start, k = 0; temp[end] != EON; end++){
			if(temp[end] == 'i'){
				initBuffer(buffer);
				memcpy(buffer, temp + start, end - start);
				val = stringToValue(buffer);
				
				prev_ptr = val->v.floatV;
				start = end;
				free(val);
			}
			if(temp[end] == 'f'){
				initBuffer(buffer);
				memcpy(buffer, temp + start, end - start);
				val = stringToValue(buffer);
				
				current_key = val->v.intV;
				free(val);

				if(key->v.intV < current_key && isLeaf == false){
					current_node = (int)(prev_ptr + 0.5);
					break;
				} 
				if(key->v.intV < current_key && isLeaf == true){
					break;
				}  
				start = end;
				
			}
		}

		if(temp[end] != EON && isLeaf == false){ // continue searching the empty spot
			unpinPage(bm, &ph);
			continue;
		}
		if(temp[end] != EON && isLeaf == true){ // if this node is a leaf node, 
							// we've found an empty spot
							// and the spot is in the middle
			for(end = end - 1; end > 0; end--){
				if(temp[end] == 'f') break;
			}
			//find the position to partition the node

			float ptr = rid.page + rid.slot * 0.1;
			val = (Value*)malloc(sizeof(Value));
			val->dt = DT_FLOAT;
			val->v.floatV = ptr;
			
			
			char* temp2 = (char*)malloc(PAGE_SIZE); // temp buffer to 
								// store the right half
			initBuffer(temp2);
			strcpy(temp2, temp + end);
			temp[end] = 0;
			strcat(temp, serializeValueModified(val)); // append the ptr

			val->dt = DT_INT;
			val->v.intV = key->v.intV;
			strcat(temp, serializeValueModified(val)); // append the key
			strcat(temp, temp2); // append the right half
			// insertion done
			printf("current node is: %s\n", temp);
			markDirty(bm, &ph);

			unpinPage(bm, &ph);
			free(val);
			free(temp2);
			break;
		}
		if(temp[end] == EON && isLeaf == false){ // continue searching the empty spot
			initBuffer(buffer);
			memcpy(buffer, temp + start, end - start);
			val = stringToValue(buffer);
			prev_ptr = val->v.floatV;
			current_node = (int)(prev_ptr + 0.5);
			free(val);
			unpinPage(bm, &ph);
			continue;
		}
		if(temp[end] == EON && isLeaf == true){ // if this node is a leaf node, 
							// we've found an empty spot
							// and the spot is at the end
			if(isNull == true) tree->numNodes = 1;
			temp[end] = 0;
			float ptr = rid.page + rid.slot * 0.1;
			
			val = (Value*)malloc(sizeof(Value));
			val->dt = DT_FLOAT;
			val->v.floatV = ptr;
			strcat(temp, serializeValueModified(val)); // append the ptr

			val->dt = DT_INT;
			val->v.intV = key->v.intV;
			strcat(temp, serializeValueModified(val)); // append the key

			temp[strlen(temp)] = EON; // set the EON
			//insertion done
			printf("current node %d: %s\n", ph.pageNum - 1, temp);
			markDirty(bm, &ph);
			unpinPage(bm, &ph);
			free(val);
			break;
		}

		free(buffer);

	}// while ends

	tree->numEntries++;


	leafSplit(tree, current_node);

	return RC_OK;
}


RC deleteKey (BTreeHandle *tree, Value *key){
	int root = 0;
	int current_node = root;

	int current_key;
	float prev_ptr;
	int parent_node;

	bool isNull;
	bool isLeaf;
	bool isRoot;

	BM_BufferPool *bm = (BM_BufferPool*)tree->mgmtData;
	
	BM_PageHandle ph;


	while(true){

		pinPage(bm, &ph, current_node + OFFSET);
		memcpy(&isNull, ph.data, sizeof(bool));
		memcpy(&isLeaf, ph.data + sizeof(bool), sizeof(bool));

		
		int start, end, k;	
		char* temp = ph.data + (2 * sizeof(bool));

		char* buffer = (char*)malloc(PAGE_SIZE);

		Value *val;
		
		end = 0;


		if(current_node != root){ // if the current node is not the root
			memcpy(&parent_node, ph.data + 2*sizeof(bool), sizeof(int));
			temp = temp + sizeof(int);
		}

		
		start = 0;
		for(end = start, k = 0; temp[end] != EON; end++){
			if(temp[end] == 'i'){
				initBuffer(buffer);
				memcpy(buffer, temp + start, end - start);
				val = stringToValue(buffer);
				
				prev_ptr = val->v.floatV;
				start = end;
				free(val);
			}
			if(temp[end] == 'f'){
				initBuffer(buffer);
				memcpy(buffer, temp + start, end - start);
				val = stringToValue(buffer);
				
				current_key = val->v.intV;
				free(val);

				if(key->v.intV < current_key && isLeaf == false){
					current_node = (int)(prev_ptr + 0.5);
					break;
				} 
				if(key->v.intV <= current_key && isLeaf == true){
					break;
				}  
				start = end;
			}
		}

		if(temp[end] != EON && isLeaf == false){ // continue searching the target key
			unpinPage(bm, &ph);
			continue;
		}
		if(temp[end] != EON && isLeaf == true){
			
			initBuffer(buffer);
			strcpy(buffer, temp + end); 	// temp[end] == 'f'
							// temp[start ~~~ end-1] = target key
							// Now, the buffer stores the tail part (needs to preserve)
			

			if(key->v.intV < current_key) return RC_IM_KEY_NOT_FOUND;
			
			for(start = end - 1; start >= 0; start--){
				if(temp[start] == 'f') break;
			}
			// now, temp[0~~~~~~start-1] is the head part which we want to preserve
			// temp[start~~~~~end-1] is the part to be discarded

			temp[start] = 0;
			strcat(temp, buffer);
			
		
			

			unpinPage(bm, &ph);
			break;
			
		}
		if(temp[end] == EON && isLeaf == false){ // continue searching the target key
			initBuffer(buffer);
			memcpy(buffer, temp + start, end - start);
			val = stringToValue(buffer);
			prev_ptr = val->v.floatV;
			current_node = (int)(prev_ptr + 0.5);
			free(val);
			unpinPage(bm, &ph);
			continue;
		}
		if(temp[end] == EON && isLeaf == true){
			unpinPage(bm, &ph);
			return RC_IM_KEY_NOT_FOUND;
		}

		free(buffer);

	}

	leafUnderFlow(tree, current_node);

	return RC_OK;
}


RC openTreeScan (BTreeHandle *tree, BT_ScanHandle **handle){

	return RC_OK;
}


RC nextEntry (BT_ScanHandle *handle, RID *result){

	return RC_OK;
}


RC closeTreeScan (BT_ScanHandle *handle){

	return RC_OK;
}

char *printTree(BTreeHandle *tree){
	char* std_out;

	return std_out;
}
