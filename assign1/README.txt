@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Manual:
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Please type "make" to compile the files.
and type "./test_assign1_1" to run the test code.

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Notes:
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
* we preserve the first 4KB of a file to store information about the file (i.e. header). So the data will start from the 2nd page rather than the beginning of the file.

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Module Description:
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


Our module is composed of three files:
page_file_controller.c: Including functions create, open, destroy file and close file.

	create: use open(), write() system calls to create a file, fill the first page with the file handler and 0 bytes, and fill the second page with the 0 bytes (2nd page is the start of the data area).
	open: use open() system call to open the file, read in the first bytes to get (initialize) the file handler information. Then, push the current file handler to the linked list which maintains the list of opend files.
	close: use close() to release the system file handler, and remove the corresponding file handler out of the linked list.
	destroy: check from the linked list if the file is currently opened. If not, use unlink() to remove the file. Otherwise, close it first and then remove the file.

block_disk_controller.c: Including functions related to READ operation.
	readBlock: read the system file handler from the book keeping info. area in SM_FileHandle, and use pread() to access the pageNum-th page of the data.
	getBlockPos: return the current page position stored in the SM_FileHandle
	readFirstBlock~readLastBlock: same as readBlock
	
block_page_controller.c: Including functions related to WRITE operation.
	writeBlock: read the system file handler from the book keeping info. area in SM_FileHandle, and use pwrite() to write to the pageNum-th page of the data.
	writeCurrentBlock: same as above
	appendEmptyBlock: generate an empty page filled with 0 bytes and write to the end of the file. Then add 1 to the totalNumPages.
	ensureCapacity: check x = numberOfPages - totalNumPages. If negative or zero, do nothing. Otherwise, call the appendEmptyBlock function for x times.



Main structure:
SM_fileHandle: containing file name, total number pages, current page position and a pointer used to store information (e.g file discriptor).

SM_OpenFile: this struct is an element in a doubly linked list which is used to keep track on the currently opened files.

SM_PageHandle: pointer used to manipulate read and write operations for a page.

SM_BookKeepingInfo: struct for the book keeping info. This will be stored in mgmtInfo field in SM_FileHandle.

	
