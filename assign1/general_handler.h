#ifndef GENERAL_HANDLER_H
#define GENERAL_HANDLER_H

#include "dberror.h"

/************************************************************
 *                    interface                             *
 ************************************************************/



extern void initBookKeepingInfo(SM_FileHandle* fHandle);	//given a file, initialize the mgmtInfo field and all requied pages


extern void pushOpenFile(SM_OpenFile *file);	//add the 'curr' to the end of the linked list

extern RC removeOpenFile(SM_OpenFile *curr);	//remove the 'curr' from the linked list



		/***  free variables ***/

extern void free_mgmtInfo(SM_FileHandle *fHandle);


#endif
