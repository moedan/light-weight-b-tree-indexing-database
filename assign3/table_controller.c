#include <stdio.h>
#include <string.h>
#include "storage_mgr.h"
#include "buffer_mgr.h"
#include "record_mgr.h"
#include "dberror.h"

RC initRecordManager (void *mgmtData){

	return RC_OK;
}
RC shutdownRecordManager (){

	return RC_OK;
}

/**
 * create table
 * @param char *: file name
 * @param Schema *: schema
 * @return RC: error control code number
 */
RC createTable (char *name, Schema *schema){
    
    //init
	SM_FileHandle fh;
	
	createPageFile(name);
	openPageFile(name, &fh);
    
    //buffer used to store schema
	char* buffer = (char*)malloc(PAGE_SIZE);
    //pages used to store schema
	int pageNumsForTableInfo = 0;	
	int buffer_index = 0;	// this is used as a pointer to current position within the buffer
	int i, j;
    
    //init buffer
	for(i = 0; i < PAGE_SIZE; i++){
		buffer[i] = 0;
	}
    
    //write the number of attributes to buffer
	memcpy(buffer + buffer_index, &(schema->numAttr), sizeof(int));
	buffer_index += sizeof(int);
    
    //write the keysize to buffer
	memcpy(buffer + buffer_index, &(schema->keySize), sizeof(int));
	buffer_index += sizeof(int);

	//write all the attribute name,data type and type length into buffer
	for(i = 0; i < schema->numAttr; i++){
		memcpy(buffer + buffer_index, schema->attrNames[i], strlen(schema->attrNames[i]) + 1);
		buffer_index += strlen(schema->attrNames[i]) + 1;
		memcpy(buffer + buffer_index, &(schema->dataTypes[i]), sizeof(DataType));
		buffer_index += sizeof(DataType);
		memcpy(buffer + buffer_index, &(schema->typeLength[i]), sizeof(int));
		buffer_index += sizeof(int);

	}
    
    //write the key attributes into buffer
	for(i = 0; i < schema->keySize; i++){
		memcpy(buffer + buffer_index, &(schema->keyAttrs[i]), sizeof(int));
		buffer_index += sizeof(int);
	}

    //write the buffer to page
	writeBlock(0, &fh, buffer);
	free(buffer);

	closePageFile(&fh);	

	return RC_OK;
}

/**
 * open table
 * @param RM_TableData: table reference
 * @param char*: file name
 * @return RC: error control code number
 */
RC openTable (RM_TableData *rel, char *name){
    
    //init
	BM_BufferPool *bm = MAKE_POOL();	
	Schema *schema = (Schema*)malloc(sizeof(Schema));	
	BM_PageHandle ph;
	
	int pageNumsForTableInfo;
	int i, j;	
	int buffer_index = 0;	// used as a pointer to the current position within a buffer
	char* buffer;
    //init the buffer pool
	initBufferPool(bm, name, 100, RS_LRU, NULL);
    
    //pin first page out to get the schema stored there
	pinPage(bm, &ph, 0);
    
	buffer = ph.data;

	rel->pageNumsForInfo = 0;
    
    //get schema info
	memcpy(&(schema->numAttr), buffer + buffer_index, sizeof(int));
	buffer_index += sizeof(int);

	memcpy(&(schema->keySize), buffer + buffer_index, sizeof(int));
	buffer_index += sizeof(int);
    
	/*	initialization	*/
	schema->attrNames = (char**)malloc(schema->numAttr * sizeof(char*));
	for(i = 0; i < schema->numAttr; i++){
		schema->attrNames[i] = (char*)malloc(200);
	}
	schema->dataTypes = (DataType*)malloc(schema->numAttr * sizeof(DataType));
	schema->typeLength = (int*)malloc(schema->numAttr * sizeof(int));
	schema->keyAttrs = (int*)malloc(schema->keySize * sizeof(int));
	/*	initialization done	*/


/*	Following two loops read the schema information from the buffer
 *	in the same order by which we wrote the information
 * */
	for(i = 0; i < schema->numAttr; i++){
		memcpy(schema->attrNames[i], buffer + buffer_index, strlen(schema->attrNames[i]) + 1);
		buffer_index += strlen(schema->attrNames[i]) + 1;
		memcpy(&(schema->dataTypes[i]), buffer + buffer_index, sizeof(DataType));
		buffer_index += sizeof(DataType);
		memcpy(&(schema->typeLength[i]), buffer + buffer_index, sizeof(int));
		buffer_index += sizeof(int);
	}
	for(i = 0; i < schema->keySize; i++){
		memcpy(&(schema->keyAttrs[i]), buffer + buffer_index, sizeof(int));
		buffer_index += sizeof(int);

	}
	unpinPage(bm, &ph);
    
    //assign the schema to table
	rel->schema = schema;
	rel->name = (char*)malloc(strlen(name) + 1);
	strcpy(rel->name, name);
	
	rel->mgmtData = bm;
    
    //get record numbers one page can store based on the schema
	rel->recordsPerPage = PAGE_SIZE / (sizeof(bool) + getRecordSize(schema));
    
	free(buffer);

	return RC_OK;
}

/**
 * close table
 * @param RM_TableData: table reference
 * @return RC: error control code number
 */
RC closeTable (RM_TableData *rel){
	
	BM_BufferPool *bm = (BM_BufferPool*)rel->mgmtData;
	

	// shutdown the pool manager first
	if(shutdownBufferPool(bm) == RC_OK){	
		freeSchema(rel->schema);
		free(rel->schema);
		free(rel->name);
		free(bm);
		return RC_OK;
	}else{
		return RC_BM_DESTROY_PINNED_PAGE;
	}
}

/**
 * delete table file
 * @param char*: file name
 * @return RC: error control code number
 */
RC deleteTable (char *name){
	destroyPageFile(name);
	return RC_OK;
}

int getNumTuples (RM_TableData *rel){
	RM_ScanHandle *sc = (RM_ScanHandle*)malloc(sizeof(RM_ScanHandle));
	Record *r = (Record*)malloc(sizeof(Record));
	int rc;

	int numTuples;

	sc->rel = rel;
	sc->mgmtData = rel->mgmtData;

	startScan(rel, sc, NULL);

	while((rc = next(sc, r)) == RC_OK){
		numTuples++;
	}
	return numTuples;
}
