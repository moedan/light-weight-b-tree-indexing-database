#include"buffer_mgr.h"
#include"storage_mgr.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
//The getFrameContents function returns an array of PageNumbers (of size numPages)
//where the ith element is the number of the page stored in the ith page frame.
//An empty page frame is represented using the constant NO_PAGE.

PageNumber *getFrameContents (BM_BufferPool *const bm){
	BM_Frame* fList =(BM_Frame*)bm->mgmtData;
	int numPages = bm->numPages;
//Allocate memory for frame contents
	PageNumber* content = (PageNumber*)malloc(bm->numPages * sizeof(PageNumber));
	if(content != NULL){
		int i;
		for(i=0; i<numPages; i++){
			content[i] = fList[i].pageNum;
		}
	} //this loop returns PageNumbers for each frame;
	return content;

}

bool *getDirtyFlags (BM_BufferPool *const bm){
//  Get frame info
	BM_Frame* fList = (BM_Frame*)bm->mgmtData;
	int numPages = bm->numPages;
// Allocate memory to store sequence of dirty pages
	bool* dirtys = (bool*)malloc(numPages * sizeof(bool));
	if (dirtys != NULL){
		int i;
		for (i = 0; i<numPages; i++){
			dirtys[i] = fList[i].isDirty;
		}
	} // this loop returns bool value which indicate which frame is dirty
	return dirtys;
}


int *getFixCounts (BM_BufferPool *const bm){
	BM_Frame* fList = (BM_Frame*)bm->mgmtData;
	int numPages = bm->numPages;
//Allocate memory for the fixCount info
	int* fixcounts = (int*)malloc(numPages * sizeof(int));
	if (fixcounts != NULL){
		int i;
		for(i=0; i<numPages; i++){
			fixcounts[i] = fList[i].fixCounts;
		}
	} // this loop returns fixCounts for each frame;
	return fixcounts;
}


int getNumReadIO (BM_BufferPool *const bm){
	return (bm->numRead);
}

int getNumWriteIO (BM_BufferPool *const bm){
	return (bm->numWrite);
}
