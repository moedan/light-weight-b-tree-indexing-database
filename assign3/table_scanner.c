#include <stdio.h>
#include "storage_mgr.h"
#include "buffer_mgr.h"
#include "record_mgr.h"


typedef struct ScanMgr
{
	int page;//store the current position of the scan handler
	int slot;
	Expr* cond;
}ScanMgr;


// scans
RC startScan (RM_TableData *rel, RM_ScanHandle *scan, Expr *cond)
{
	scan->rel=rel;
	ScanMgr* scanMgr=(ScanMgr*)malloc(sizeof(ScanMgr));
	scanMgr->cond=cond;
	scanMgr->page=1;//initialize the first scan position to be page 1, slot 0
	scanMgr->slot=0;
	scan->mgmtData=scanMgr;
	return RC_OK;
}

RC next (RM_ScanHandle *scan, Record *record){
	//start searching from the first record;
	ScanMgr* scanMgr = (ScanMgr *) scan->mgmtData;
	Expr* cond = scanMgr->cond;
	//int numofTuple =  getNumTuples(scan->rel);
	RM_TableData* rel=scan->rel;
	BM_BufferPool* bm = (BM_BufferPool*)rel->mgmtData;
	BM_PageHandle h;
	SM_FileHandle fh;
	openPageFile(rel->name, &fh);
	int curPos = rel->pageNumsForInfo;

	bool isFound = FALSE;
	RID id;

	for (; scanMgr->page < fh.totalNumPages; scanMgr->page++) {
		int recordsPerPage = rel->recordsPerPage;
		fh.curPagePos = scanMgr->page;
		CHECK(pinPage(bm,&h,fh.curPagePos));
		char* data = h.data;

		for (; scanMgr->slot < recordsPerPage; scanMgr->slot++) {
			id.page = scanMgr->page;
			id.slot = scanMgr->slot;
			getRecord(rel, id, record);
			Value* value;
			CHECK(evalExpr(record, rel->schema, cond, &value));
			if (value->v.boolV) {
				isFound = TRUE;
				scanMgr->slot++;
				break;
			}
		}
		CHECK(unpinPage(bm, &h));
		if (isFound)
			break;
	}
	CHECK(closePageFile(&fh));
	if(isFound)
			return RC_OK;
		else
			return RC_RM_NO_MORE_TUPLES;
}


RC closeScan (RM_ScanHandle *scan)
{
	ScanMgr* scanMgr=(ScanMgr*)scan->mgmtData;
	free(scan->mgmtData);
	scan->mgmtData=NULL;
	return RC_OK;
}

