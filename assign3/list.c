#include "list.h"
#include <stdio.h>
#include <stdlib.h>


List* list_new(void){
  List* s = malloc( 1 * sizeof(*s));               
  if( NULL == s ) printf("new list failed");
  s->head = s->tail = NULL;
  return s;
}

List* list_add_element(List* s, const int index){
  Node* p = malloc( 1 * sizeof(*p) );

  if( NULL == p ){
    printf("fail to malloc node\n");
    return s; 
  }

  p->cache_index = index;                 //init node
  p->next = NULL;

  if( NULL == s ){
      printf("FIFO List not initialized\n");
      free(p);
      return s;
  }
  else if( NULL == s->head && NULL == s->tail ){  // list is empty
    s->head = s->tail = p;
    return s;
  }
  else{
    s->tail->next = p;
    s->tail = p;
  }
  return s;
}

List* list_remove_element_X( List* s , const int index ){ 
  Node* prevP, * currP;
  
  if( NULL == s ){
    printf("FIFO List is not initialized\n");
    return s;
  }
  else if( NULL == s->head && NULL == s->tail ){
    printf(" List is empty\n");
    return s;
  }

  /* For 1st node, there is no previous node. */
  prevP = NULL;

  /* Visit each node, maintaining a pointer to the previous node we just visited.*/
  for (currP = s->head;	currP != NULL;prevP = currP, currP = currP->next) {

    if (currP->cache_index == index) {  /* Found it. */
      if (prevP == NULL){           /* Fixing beginning problem. */
	s->head = currP->next;
	if( NULL == s->head ) s->tail = s->head; /* in case list has only one element */
      }else {
         /* Fix previous node's next to skip over the removed node.*/
        prevP->next = currP->next;
      }

      /* Deallocate the node. */
      free(currP);
    }
  } 
  return s;
}

/* remove the first node */
List* list_remove_element( List* s){     
  Node* h, *p;
  
  if( NULL == s ){
    printf("FIFO List is not initialized\n");
    return s;
  }
  else if( NULL == s->head && NULL == s->tail ){
    printf("FIFO List is empty\n");
    return s;
  }
  
  h = s->head;
  p = h->next;
  free(h);
  s->head = p;
  if(s->head == NULL) s->tail = s->head; 
  return s;
}


List* list_free( List* s ){
  while( s->head!=NULL ){
    list_remove_element(s);
  }
  return s;
}
