#include <stdio.h>
#include "storage_mgr.h"
#include "buffer_mgr.h"
#include "record_mgr.h"

static RC attrOffSet (Schema *schema, int attrNum, int *result);
static int getTypeLength(Schema * schema, int attrNum);
static Value* stringToVal(DataType dt,char *str, int length);

/**
 * insert Record
 * @param RM_TableData *: table reference
 * @param Record*: record
 * @return RC: code for error control
 */
RC insertRecord (RM_TableData *rel, Record *record){
    
    //init
	BM_BufferPool* bm = (BM_BufferPool*)rel->mgmtData;
	BM_PageHandle h;
	SM_FileHandle fh;
    
	int i;
	bool isFilled;
	int curPos = rel->pageNumsForInfo + 1;
    //pin the page out
	pinPage(bm, &h, curPos);
    
	int recordsPerPage = rel->recordsPerPage;
	int recordStartAddress = recordsPerPage * sizeof(bool);
    
    //find the available slot to store the record
	for(i = 0; i < recordsPerPage; i++){
		memcpy(&isFilled, h.data + i * sizeof(bool), sizeof(bool));
		if(isFilled == false) break;
	}
	while(i == recordsPerPage){
		unpinPage(bm, &h);
		curPos++;
		pinPage(bm, &h, curPos);
		for(i = 0; i < recordsPerPage; i++){
			memcpy(&isFilled, h.data + i * sizeof(bool), sizeof(bool));
			if(isFilled == false) break;
		}
	}
    //assign the page number and slot number to record
	record->id.page = curPos;
	record->id.slot = i;
	
    //write the record data to page
	memcpy(h.data + recordStartAddress + record->id.slot * getRecordSize(rel->schema), record->data, getRecordSize(rel->schema));
    //set the slot indicator as filled
	isFilled = true;
    //write the indicator to page
	memcpy(h.data + record->id.slot * sizeof(bool), &isFilled, sizeof(bool));
    
    //mark dirty and unpin the page
    markDirty(bm, &h);
	unpinPage(bm, &h);
    
	return RC_OK;
}

/**
 * delete record
 * @param RM_TableData*: table reference
 * @param RID: record id
 * @return RC: code for error control
 */
RC deleteRecord (RM_TableData *rel, RID id){
    //init
    BM_BufferPool* bm = (BM_BufferPool*)rel->mgmtData;
    BM_PageHandle h;
    
    bool isFilled;
    
    //pin the page out
    pinPage(bm, &h, id.page);
    
    //check whether the slot has record based on the indicator
    memcpy(&isFilled, h.data + id.slot * sizeof(bool), sizeof(bool));
    
    if (isFilled==false) {
        //if the indicator shows no record, return error
        unpinPage(bm, &h);
        return RC_RM_NO_SUCH_RECORD;
    }
    else{
        //if the indicator shows there's such record, set the indicator as false but don't remove the record data
		isFilled = false;
		memcpy(h.data + id.slot * sizeof(bool), &isFilled, sizeof(bool));
        //as the page is edited, markDirty then unpin
		markDirty(bm, &h);
        unpinPage(bm, &h);
        return RC_OK;
    }
    
}

/**
 * update Record
 * @param RM_TableData *: table reference
 * @param Record*: record
 * @return RC: code for error control
 */
RC updateRecord (RM_TableData *rel, Record *record){
    
    //init
	BM_BufferPool* bm = (BM_BufferPool*)rel->mgmtData;
	BM_PageHandle h;
    
	int recordStartAddress = rel->recordsPerPage * sizeof(bool);
    //pin the page out
	pinPage(bm, &h, (record->id).page);
    //update the data based on rid
	memcpy(h.data + recordStartAddress + (record->id).slot * getRecordSize(rel->schema), record->data, getRecordSize(rel->schema));
    //mark dirty and unpin page
	markDirty(bm, &h);
	unpinPage(bm, &h);
    
    
	return RC_OK;
}

/**
 * get Record
 * @param RM_TableData *: table reference
 * @param RID: record id
 * @param Record*: record
 * @return RC: code for error control
 */
RC getRecord (RM_TableData *rel, RID id, Record *record){
    
    //init
	BM_BufferPool* bm = (BM_BufferPool*)rel->mgmtData;
	BM_PageHandle h;
    
	int recordStartAddress = rel->recordsPerPage * sizeof(bool);
    
    //pin the page out
	pinPage(bm, &h, id.page);
    
    //if there's so such record
    bool isFilled;
    memcpy(&isFilled, h.data + id.slot * sizeof(bool), sizeof(bool));
	if (isFilled == false) {
		unpinPage(bm, &h);
		return RC_RM_NO_SUCH_RECORD;
	}
    
    //get the record value
	(record->id).page = id.page;
	(record->id).slot = id.slot;
    
	memcpy(record->data, h.data + recordStartAddress + id.slot * getRecordSize(rel->schema), getRecordSize(rel->schema));
    
    //unpin the page
	unpinPage(bm, &h);
    
	return RC_OK;
}

/**
 * create Record
 * @param Record **: pointer to pointer to record
 * @param Schema *: schema
 * @return RC: code for error control
 */
RC createRecord (Record **record, Schema *schema){
	
	(*record) = (Record *)malloc(sizeof(Record));
	int recordSize = getRecordSize(schema);
	(*record)->data = (char *)malloc(recordSize);
	(*record)->id.page = 0;
	(*record)->id.slot = 0;
    
    
	return RC_OK;
}

/**
 * free record
 * @param Record: the record
 * @return RC: code for error control
 */
RC freeRecord (Record *record){
    
	free(record->data);
	free(record);
	return RC_OK;
}

/**
 * get attribute
 * @param Record*: record
 * @param Schema*: user-defined schema
 * @param int: attribute number
 * @param Value**: value list pointer
 * @return RC: code for error control
 */
RC getAttr (Record *record, Schema *schema, int attrNum, Value **value){
    
    //get offset
	int offset=0;
	RC RC_CODE = attrOffSet(schema, attrNum, &offset);
	if (RC_CODE != RC_OK) return RC_CODE;
	
    //get length
	int length = getTypeLength(schema,attrNum);
	
    //get the value stored in record in string format
	char * attrData=malloc(length);
	memcpy(attrData,record->data+offset,length);
	
    //convert it to Value format
	(*value)=stringToVal(schema->dataTypes[attrNum],attrData,length);
	free(attrData);
	return RC_OK;
	
}

/**
 * set attribute
 * @param Record*: record
 * @param Schema*: user-defined schema
 * @param int: attribute number
 * @param Value*: value pointer
 * @return RC: code for error control
 */
RC setAttr (Record *record, Schema *schema, int attrNum, Value *value){
	//if the datatype in value is not the same as in schema, return error
	if(schema->dataTypes[attrNum] != value->dt) return RC_RM_DATATYPE_MISMATCH;
	
	int offset=0;
	int length = getTypeLength(schema,attrNum);
	
	RC RC_CODE = attrOffSet(schema, attrNum, &offset);
	if (RC_CODE != RC_OK) return RC_CODE;
    
    
	char *val = (char*)malloc(length);
    
	switch(value->dt){
            
        case DT_INT:
            memcpy(val, &(value->v.intV), length);
            break;
        case DT_STRING:
            memcpy(val, value->v.stringV, length);
            break;
        case DT_FLOAT:
            memcpy(val, &(value->v.floatV), length);
            break;
        case DT_BOOL:
            memcpy(val, &(value->v.boolV), length);
            break;
	}
	memcpy(record->data+offset, val,length);
	free(val);
	return RC_OK;
	
}

/**
 * find attribute start position in schema
 * @param Schema*: user-defined schema
 * @param int: attribute number
 * @param int*: result pointer
 * @return : RC: code for error control
 */
RC attrOffSet (Schema *schema, int attrNum, int *result)
{
    int offset = 0;
    int attrPos = 0;
    
    for(attrPos = 0; attrPos < attrNum; attrPos++)
        switch (schema->dataTypes[attrPos])
    {
        case DT_STRING:
            offset += schema->typeLength[attrPos];
            break;
        case DT_INT:
            offset += sizeof(int);
            break;
        case DT_FLOAT:
            offset += sizeof(float);
            break;
        case DT_BOOL:
            offset += sizeof(bool);
            break;
    }
    
    *result = offset;
    return RC_OK;
}

/**
 * return Datatype length given by user
 * @param Schema*: user-defined schema
 * @param int: attribute positon
 * @return int: length of DataType
 */
int getTypeLength(Schema * schema, int attrNum){
	switch(schema->dataTypes[attrNum]){
		case DT_INT:
			return sizeof(int);
			break;
		case DT_STRING:
			return schema->typeLength[attrNum];
		case DT_FLOAT:
			return sizeof(float);
		case DT_BOOL:
			return sizeof(bool);
	}
}

/**
 * convert a string to Value
 * @param DataType: the dataType
 * @param char*: the string pointer
 * @param int: string length
 * @return Value
 */
Value* stringToVal(DataType dt,char *str, int length){
    Value *result = (Value *) malloc(sizeof(Value));
    
    switch(dt)
    {
        case DT_INT:
            result->dt = DT_INT;
            memcpy(&result->v.intV, str, sizeof(int));
            break;
        case DT_FLOAT:
            result->dt = DT_FLOAT;
            memcpy(&result->v.floatV, str, sizeof(float));
            break;
        case DT_STRING:
            result->dt = DT_STRING;
            result->v.stringV = malloc(length + 1);
            memcpy(result->v.stringV, str, length);
            (result->v.stringV)[length] = 0;
            break;
        case DT_BOOL:
            result->dt = DT_BOOL;
            memcpy(&result->v.boolV, str, sizeof(bool));
            break;
    }
	return result;
}
